<?php
class M_admin extends CI_Model {
    // table name
    function __construct()
    {
        parent::__construct();
    }
    
	public function ambil_penjualan($bln,$yr){
		$this->db->select('AVG(harga_jual) as jual,AVG(harga_beli) as beli');
		$this->db->where('DATE_FORMAT(tanggal_iat,"%Y")', $yr);
		$this->db->where('DATE_FORMAT(tanggal_iat,"%m")', $bln);
		$query = $this->db->get('stok')->row();;
		
		return $query;
	}
	
	public function ambil_penjualan_agent($bln,$yr,$admin){
		$this->db->select('AVG(total_harga) as jual');
		$this->db->where('DATE_FORMAT(tanggal_iat,"%Y")', $yr);
		$this->db->where('DATE_FORMAT(tanggal_iat,"%m")', $bln);
		$this->db->where('id_member', $admin);
		$this->db->where('status', '3');
		$query = $this->db->get('order')->row();;
		
		return $query->jual;
	}
	
	function edit_admin($id){
      $this->db->select('*')
      ->where('id_admin',$this->myencryption->decode($id));
      return $this->db->get('adm_user')->row();
    }
	}
?>
