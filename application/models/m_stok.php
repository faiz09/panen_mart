<?php
class M_stok extends CI_Model {
    // table name
    function __construct()
    {
        parent::__construct();
    }
	
	 function add_stok($data){
      $this->db->insert('stok', $data);
      return $this->db->affected_rows(); 	
    }
	
	public function ambil_produk($ctg){
		$this->db->select('id_produk, nama');
		$this->db->where('kategori', $ctg);
		$this->db->where('status', '1');
		$this->db->order_by("tanggal_iat","asc");
		$sql=$this->db->get('produk');
		$result = array('' => 'Pilih Produk');
		if($sql->num_rows()>0){
			foreach ($sql->result_array() as $row)
			{
				$result[$row['id_produk']]= $row['nama'];
			}
		}
		return $result;
	}
	
	public function ambil_category(){
		$this->db->select('id_category, nama');
		$this->db->where('status', '1');
		$this->db->order_by("tanggal_iat","asc");
		$sql=$this->db->get('categori');
		$result = array('' => 'Pilih Kategori');
		if($sql->num_rows()>0){
			foreach ($sql->result_array() as $row)
			{
				$result[$row['id_category']]= $row['nama'];
			}
		}
		return $result;
	}
	
	public function ambil_satuan($p){
		$this->db->select('satuan');
		$this->db->where('id_produk', $p);
		$sql=$this->db->get('produk')->result();
		if($sql){
			$result = $sql[0]->satuan;
			//$result = $this->db->get('produk')->num_rows();
		}else{
			$result = '';
		}
		return $result;
	}
	
	public function ambil_agent(){
		$this->db->select('id_member, nama');
		$this->db->where('flag', '1');
		$this->db->where('level', 'agent');
		$this->db->order_by("tanggal_iat","asc");
		$sql=$this->db->get('member');
		if($sql->num_rows()>0){
			foreach ($sql->result_array() as $row)
			{
				$result[$row['id_member']]= $row['nama'];
			}
		}
		return $result;
	}
	
	function get_stok(){
      $this->db->select('stok.id_stok,categori.nama as categori,produk.nama as produk,produk.satuan,member.nama as agent,stok.harga_jual,stok.harga_beli,stok.jumlah,stok.status,stok.tanggal_iat');
	  $this->db->order_by("tanggal_iat","desc");
	  $this->db->from('stok');
	  $this->db->join('categori', 'categori.id_category = stok.id_category', 'left');
	  $this->db->join('produk', 'produk.id_produk = stok.id_produk', 'left');
	  $this->db->join('member', 'member.id_member = stok.id_agent', 'left');
      $tampung=$this->db->get()->result();
	  if($tampung){
      foreach($tampung as $temp):
      $temp1[]=array("category"=>$temp->categori,
                     "product"=>$temp->produk,
					 "satuan"=>$temp->satuan,
                     "agent"=>$temp->agent,
					 "harga_jual"=>$temp->harga_jual,
					 "harga_beli"=>$temp->harga_beli,
					 "jumlah"=>$temp->jumlah,
					 "status"=>$temp->status,
					 "tanggal"=>$temp->tanggal_iat,
                     "idEn"=>$this->myencryption->encode($temp->id_stok));
      endforeach;

      return json_decode(json_encode($temp1),false);
	  }
    }
	function delete($id){
      $this->db->delete("stok", array("id_stok"=>$this->myencryption->decode($id)));
      return $this->db->affected_rows();
    }
	
    function update_stok($data1, $id){
      $ids = $this->myencryption->decode($id);
	  $this->db->where('id_stok', $ids);
      $this->db->update('stok', $data1);
      return $this->db->affected_rows();
    }
	
	function edit_stok($id){
      $this->db->select('*')
      ->where('id_stok',$this->myencryption->decode($id));
      return $this->db->get('stok')->row();
    }
	
	public function nama_produk($p){
		$this->db->select('nama');
		$this->db->where('id_produk', $p);
		$sql=$this->db->get('produk')->row();
		if($sql){
			$result = $sql->nama;
		}else{
			$result = '';
		}
		return $result;
	}
	
	function get_stok2($agent){
      $this->db->select('stok.id_stok,categori.nama as categori,produk.nama as produk,produk.satuan,member.nama as agent,stok.harga_jual,stok.harga_beli,stok.jumlah,stok.status,stok.tanggal_iat');
	  $this->db->order_by("tanggal_iat","desc");
	  $this->db->where('id_agent', $agent);
	  $this->db->from('stok');
	  $this->db->join('categori', 'categori.id_category = stok.id_category', 'left');
	  $this->db->join('produk', 'produk.id_produk = stok.id_produk', 'left');
	  $this->db->join('member', 'member.id_member = stok.id_agent', 'left');
      $tampung=$this->db->get()->result();
	  if($tampung){
      foreach($tampung as $temp):
      $temp1[]=array("category"=>$temp->categori,
                     "product"=>$temp->produk,
					 "satuan"=>$temp->satuan,
                     "agent"=>$temp->agent,
					 "harga_jual"=>$temp->harga_jual,
					 "harga_beli"=>$temp->harga_beli,
					 "jumlah"=>$temp->jumlah,
					 "status"=>$temp->status,
					 "tanggal"=>$temp->tanggal_iat,
                     "idEn"=>$this->myencryption->encode($temp->id_stok));
      endforeach;

      return json_decode(json_encode($temp1),false);
	  }
    }
	}
?>
