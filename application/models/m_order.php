<?php
class M_order extends CI_Model {
    // table name
    function __construct()
    {
        parent::__construct();
    }
    function get_order(){
	  $this->db->select('a.id_order,a.kode_transaksi,b.nama as member,a.cart,a.total_harga,a.biaya_pengiriman,a.metode_pembayaran,a.status');
	  $this->db->order_by("a.tanggal_iat","desc");
	  $this->db->from('order a');
	  $this->db->join('member b', 'b.id_member = a.id_member', 'left');
	  //$this->db->join('member c', 'c.id_member = a.id_agent');
      $tampung=$this->db->get()->result();	
	  if($tampung){
		  foreach($tampung as $temp):
		  $temp1[]=array("member"=>$temp->member,
						 "agent"=>'',
						 "cart"=>$temp->cart,
						 "kode"=>$temp->kode_transaksi,
						 "total_harga"=>$temp->total_harga,
						 "kirim"=>$temp->biaya_pengiriman,
						 "metode"=>$temp->metode_pembayaran,
						 "status"=>$temp->status,
						 "idEn"=>$this->myencryption->encode($temp->id_order));
		  endforeach;

		  return json_decode(json_encode($temp1),false);
	  }
    }
	
	function get_order_detail($id){
	  $this->db->select('a.id_order,b.nama as member,a.cart,a.total_harga,a.biaya_pengiriman,a.metode_pembayaran,a.status,a.kode_transaksi,a.telepon,a.kecamatan,a.kabupaten,a.provinsi,a.alamat,a.tanggal_iat,a.bukti_struk,a.lat,a.lon');
	  $this->db->order_by("a.tanggal_iat","desc");
	  $this->db->where('a.id_order',$this->myencryption->decode($id));
	  $this->db->from('order a');
	  $this->db->join('member b', 'b.id_member = a.id_member', 'left');
	  //$this->db->join('member c', 'c.id_member = a.id_agent');
      $tampung=$this->db->get()->result_array();	

      foreach($tampung as $temp):
      $temp1[]=array("member"=>$temp['member'],
                     "agent"=>'',
                     "cart"=>$temp['cart'],
					 "total_harga"=>$temp['total_harga'],
					 "kirim"=>$temp['biaya_pengiriman'],
					 "metode"=>$temp['metode_pembayaran'],
					 "status"=>$temp['status'],
					 "kode_transaksi"=>$temp['kode_transaksi'],
					 "telepon"=>$temp['telepon'],
					 "kecamatan"=>$temp['kecamatan'],
					 "kabupaten"=>$temp['kabupaten'],
					 "provinsi"=>$temp['provinsi'],
					 "alamat"=>$temp['alamat'],
					 "lat"=>$temp['lat'],
					 "lon"=>$temp['lon'],
					 "struk"=>$temp['bukti_struk'],
					 "tanggal_iat"=>$temp['tanggal_iat'],
                     "idEn"=>$this->myencryption->encode($temp['id_order']));
      endforeach;

      return json_decode(json_encode($temp1),false);
    }

	
	function get_nama_barang($id){
      $this->db->select('b.nama,a.harga_jual as harga,b.satuan,c.nama as category,b.gambar')
      ->where('a.id_stok',$id);
	  $this->db->from('stok a');
	  $this->db->join('produk b', 'b.id_produk = a.id_produk', 'left');
	  $this->db->join('categori c', 'c.id_category = a.id_category', 'left');
      return $this->db->get()->row_array();
    }
	
	function update_status($id, $val){
	  $ids = $this->myencryption->decode($id);
	  $this->db->set('status', $val);
	  $this->db->where('id_order', $ids);
      $this->db->update('order');
      return $this->db->affected_rows();
    }
	
	function cek_status_metode($id){
	  $ids = $this->myencryption->decode($id);
	  $this->db->select('metode_pembayaran');
	  $this->db->where('id_order', $ids);
      $sak = $this->db->get('order')->row_array();	
      return $sak['metode_pembayaran'];
    }
	
	function cek_status_struk($id){
	  $ids = $this->myencryption->decode($id);
	  $this->db->select('bukti_struk');
	  $this->db->where('id_order', $ids);
      $sak = $this->db->get('order')->row_array();	
      return $sak['bukti_struk'];
    }
	
	public function get_agentq($id){
		$this->db->select('b.nama');
		$this->db->where('a.id_stok', $id);
		$this->db->from('stok a');
		$this->db->join('member b', 'b.id_member = a.id_agent', 'left');
		$sql=$this->db->get()->result();
		if($sql){
			$result = $sql[0]->nama;
			//$result = $this->db->get('produk')->num_rows();
		}else{
			$result = '';
		}
		return $result;
	}
	/*
    function update_blog($data1, $id){
      $ids = $this->myencryption->decode($id);
	  $this->db->where('id_berita', $ids);
      $this->db->update('berita', $data1);
      return $this->db->affected_rows();
    }/*
    function add_pasien($data){
      $this->db->insert('data_pasien', $data);
      return $this->db->affected_rows();
    }
	function edit_blog($id){
      $this->db->select('*')
      ->where('id_berita',$this->myencryption->decode($id));
      return $this->db->get('berita')->row();
    }
	
	function add_blog($data){
      $this->db->insert('berita', $data);
      return $this->db->affected_rows();
    }*/
	}
?>
