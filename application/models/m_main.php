<?php
class M_main extends CI_Model {
    // table name
    function __construct()
    {
        parent::__construct();
    }
    function get_blogs(){
      $this->db->select('*');
	  $this->db->where('status', '1');
	  $this->db->order_by('tanggal_iat', 'DESC');
      $tampung=$this->db->get('berita')->result();
	  if($tampung){
      foreach($tampung as $temp):
      $temp1[]=array("judul"=>$temp->judul,
                     "tanggal"=>$temp->tanggal_iat,
                     "penulis"=>$temp->penulis,
					 "pic"=>$temp->potret,
                     "idEn"=>$temp->slug);
      endforeach;

      return json_decode(json_encode($temp1),false);
	  }
    }
	
	function get_blogku($id){
      $this->db->select('*')
      ->where('slug',$id);
      return $this->db->get('berita')->row();
    }
	
	function get_recent(){
      $this->db->select('*');
	  $this->db->where('status', '1');
	  $this->db->order_by('tanggal_iat', 'DESC');  
	  $this->db->from('berita');
	  $this->db->limit('3');
      $tampung=$this->db->get()->result();
	  if($tampung){
      foreach($tampung as $temp):
      $temp1[]=array("judul"=>$temp->judul,
                     "tanggal"=>$temp->tanggal_iat,
                     "penulis"=>$temp->penulis,
					 "pic"=>$temp->potret,
                     "idEn"=>$temp->slug);
      endforeach;

      return json_decode(json_encode($temp1),false);
	  }
    }
	
	function get_slide(){
      $this->db->select('picture');  
	  $this->db->from('slide_picture');
	  $this->db->order_by('tanggal', 'ASC');
      $tampung=$this->db->get()->result();
	  if($tampung){
      foreach($tampung as $temp):
      $temp1[]=array("picture"=>$temp->picture);
      endforeach;

      return json_decode(json_encode($temp1),false);
	  }
    }
	
    /*function delete($id){
      $this->db->delete("berita", array("id_berita"=>$this->myencryption->decode($id)));
      return $this->db->affected_rows();
    }
	
    function update_blog($data1, $id){
      $ids = $this->myencryption->decode($id);
	  $this->db->where('id_berita', $ids);
      $this->db->update('berita', $data1);
      return $this->db->affected_rows();
    }/*
    function add_pasien($data){
      $this->db->insert('data_pasien', $data);
      return $this->db->affected_rows();
    }
	
	function add_($data){
      $this->db->insert('berita', $data);
      return $this->db->affected_rows();
    }*/
	}
?>
