<?php
class M_member extends CI_Model {
    // table name
    function __construct()
    {
        parent::__construct();
    }
	
	
	function get_member(){
      $this->db->select('*');
	  $this->db->order_by("tanggal_iat","desc");
      $tampung=$this->db->get('member')->result();
	  if($tampung){
      foreach($tampung as $temp):
      $temp1[]=array("nama"=>$temp->nama,
                     "telepon"=>$temp->telepon,
                     "kelamin"=>$temp->kelamin,
					 "email"=>$temp->email,
					 "alamat"=>$temp->alamat,
					 "gambar"=>$temp->foto,
					 "level"=>$temp->level,
					 "status"=>$temp->flag,
                     "idEn"=>$this->myencryption->encode($temp->id_member));
      endforeach;

      return json_decode(json_encode($temp1),false);
	   }
    }
	
	function update_member($data1, $id){
      $ids = $this->myencryption->decode($id);
	  $this->db->where('id_member', $ids);
      $this->db->update('member', $data1);
      return $this->db->affected_rows();
    }
	function edit_member($id){
      $this->db->select('*')
      ->where('id_member',$this->myencryption->decode($id));
      return $this->db->get('member')->row();
    }
	
	function delete($id){
      $this->db->delete("member", array("id_member"=>$this->myencryption->decode($id)));
      return $this->db->affected_rows();
    }
	
	function pic($id){
      $this->db->select('foto')
      ->where('id_member',$this->myencryption->decode($id));
      return $this->db->get('member')->row();
    }
	/* function add_product($data){
      $this->db->insert('produk', $data);
      return $this->db->affected_rows();
    }
	
	public function ambil_category(){
		$this->db->select('id_category, nama');
		$this->db->order_by("urutan","asc");
		$sql=$this->db->get('categori');
		if($sql->num_rows()>0){
			foreach ($sql->result_array() as $row)
			{
				$result[$row['id_category']]= $row['nama'];
			}
		}
		return $result;
	}*/
	
	/*function deactive($ids){
	  $idq = $this->myencryption->decode($ids);
	  $this->db->set('status', '2');
	  $this->db->where('id_produk', $idq);
      $this->db->update('produk');
      return $this->db->affected_rows();
    }
	*/
	}
?>
