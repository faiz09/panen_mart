<?php
class M_request extends CI_Model {
    // table name
    function __construct()
    {
        parent::__construct();
    }
	
    function get_request(){
      $this->db->select('req_agent.id_member,member.nama as member,adm_user.userNama as admin,req_agent.status,req_agent.tanggal_iat,req_agent.tanggal_uat');
	  $this->db->order_by("req_agent.tanggal_iat","desc");
	  $this->db->from('req_agent');
	  $this->db->join('member', 'member.id_member = req_agent.id_member');
	  $this->db->join('adm_user', 'adm_user.id_admin = req_agent.id_admin', 'left');
      $tampung=$this->db->get()->result();
	  if($tampung){
      foreach($tampung as $temp):
      $temp1[]=array("member"=>$temp->member,
                     "admin"=>$temp->admin,
                     "status"=>$temp->status,
					 "tanggal_iat"=>$temp->tanggal_iat,
					 "tanggal_uat"=>$temp->tanggal_uat,
                     "idEn"=>$this->myencryption->encode($temp->id_member));
      endforeach;

      return json_decode(json_encode($temp1),false);
	  }
    }
	
	function approve($id,$admin){
	  $this->db->set('status', '1');
	  $this->db->set('id_admin', $admin);
	  $this->db->where('id_member',$this->myencryption->decode($id));
      $this->db->update('req_agent');
      return $this->db->affected_rows();
    }
	
	function to_admin($id){
	  $this->db->set('level', 'agent');
	  $this->db->where('id_member',$this->myencryption->decode($id));
      $this->db->update('member');
      return $this->db->affected_rows();
    }
	}
?>
