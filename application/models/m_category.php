<?php
class M_category extends CI_Model {
    // table name
    function __construct()
    {
        parent::__construct();
    }
	
	function add_max(){
	  $this->db->select_max('urutan');
	  $user = $this->db->get('categori')->row();
	  $r = $user->urutan+1;
	  return $r;
    }
	 function add_category($data){
      $this->db->insert('categori', $data);
      return $this->db->affected_rows();
    }
	
    function get_category(){
      $this->db->select('*');
	  $this->db->order_by("urutan","asc");
      $tampung=$this->db->get('categori')->result();
	  if($tampung){
      foreach($tampung as $temp):
      $temp1[]=array("nama"=>$temp->nama,
                     "tanggal"=>$temp->tanggal_iat,
                     "slug"=>$temp->slug,
					 "gambar"=>$temp->gambar,
					 "urutan"=>$temp->urutan,
					 "status"=>$temp->status,
                     "idEn"=>$this->myencryption->encode($temp->id_category));
      endforeach;

      return json_decode(json_encode($temp1),false);
	  }
    }
	
    function delete($id){
      $this->db->delete("categori", array("id_category"=>$this->myencryption->decode($id)));
      return $this->db->affected_rows();
    }
	function pic($id){
      $this->db->select('gambar')
      ->where('id_category',$this->myencryption->decode($id));
      return $this->db->get('categori')->row();
    }
    function update_category($data1, $id){
      $ids = $this->myencryption->decode($id);
	  $this->db->where('id_category', $ids);
      $this->db->update('categori', $data1);
      return $this->db->affected_rows();
    }
	function edit_category($id){
      $this->db->select('*')
      ->where('id_category',$this->myencryption->decode($id));
      return $this->db->get('categori')->row();
    }
	public function ambil_urutan(){
		$this->db->select('urutan');
		$this->db->order_by("urutan","asc");
		$sql=$this->db->get('categori');
		if($sql->num_rows()>0){
			foreach ($sql->result_array() as $row)
			{
				$result[$row['urutan']]= $row['urutan'];
			}
		}
		return $result;
	}
	function swap($urutan_b, $urutan){
	  $this->db->set('urutan', $urutan);
	  $this->db->where('urutan', $urutan_b);
      $this->db->update('categori');
      return $this->db->affected_rows();
    }
	function swap_all($urutan_b, $urutan){
		$SQL = "UPDATE
		categori AS urutan1
		JOIN categori AS urutan2 ON
		(urutan1.urutan = '$urutan_b' AND urutan2.urutan = '$urutan')
		SET
		urutan1.urutan = urutan2.urutan,
		urutan2.urutan = urutan1.urutan";
		
		$query = $this->db->query($SQL);
	}
	}
?>
