<?php
class M_product extends CI_Model {
    // table name
    function __construct()
    {
        parent::__construct();
    }
	
	 function add_product($data){
      $this->db->insert('produk', $data);
      return $this->db->affected_rows();
    }
	
	public function ambil_category(){
		$this->db->select('id_category, nama');
		$this->db->order_by("urutan","asc");
		$this->db->where('status', '1');
		$sql=$this->db->get('categori');
		if($sql->num_rows()>0){
			foreach ($sql->result_array() as $row)
			{
				$result[$row['id_category']]= $row['nama'];
			}
		}
		return $result;
	}
	
	function get_product(){
      $this->db->select('produk.id_produk,produk.nama,produk.satuan,categori.nama as kategori,produk.gambar,produk.status,produk.tanggal_iat');
	  $this->db->order_by("tanggal_iat","desc");
	  $this->db->from('produk');
	  $this->db->join('categori', 'categori.id_category = produk.kategori', 'left');
      $tampung=$this->db->get()->result();
	  if($tampung){
      foreach($tampung as $temp):
      $temp1[]=array("nama"=>$temp->nama,
                     "tanggal"=>$temp->tanggal_iat,
					 "gambar"=>$temp->gambar,
					 "satuan"=>$temp->satuan,
					 "category"=>$temp->kategori,
					 "status"=>$temp->status,
                     "idEn"=>$this->myencryption->encode($temp->id_produk));
      endforeach;

      return json_decode(json_encode($temp1),false);
	   }
    }
	function deactive($ids){
	  $idq = $this->myencryption->decode($ids);
	  $this->db->set('status', '2');
	  $this->db->where('id_produk', $idq);
      $this->db->update('produk');
      return $this->db->affected_rows();
    }
	function delete($id){
      $this->db->delete("produk", array("id_produk"=>$this->myencryption->decode($id)));
      return $this->db->affected_rows();
    }
	function pic($id){
      $this->db->select('gambar')
      ->where('id_produk',$this->myencryption->decode($id));
      return $this->db->get('produk')->row();
    }
	
    function update_product($data1, $id){
      $ids = $this->myencryption->decode($id);
	  $this->db->where('id_produk', $ids);
      $this->db->update('produk', $data1);
      return $this->db->affected_rows();
    }
	function edit_product($id){
      $this->db->select('*')
      ->where('id_produk',$this->myencryption->decode($id));
      return $this->db->get('produk')->row();
    }
	}
?>
