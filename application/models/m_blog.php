<?php
class M_blog extends CI_Model {
    // table name
    function __construct()
    {
        parent::__construct();
    }
    function get_blogs(){
      $this->db->select('*');
	  $this->db->order_by("tanggal_iat","desc");
      $tampung=$this->db->get('berita')->result();
	  if($tampung){
      foreach($tampung as $temp):
      $temp1[]=array("judul"=>$temp->judul,
                     "tanggal"=>$temp->tanggal_iat,
                     "penulis"=>$temp->penulis,
					 "isi"=>$temp->isi,
					 "pic"=>$temp->potret,
					 "status"=>$temp->status,
                     "idEn"=>$this->myencryption->encode($temp->id_berita));
      endforeach;

      return json_decode(json_encode($temp1),false);
	  }
    }
	
    function delete($id){
      $this->db->delete("berita", array("id_berita"=>$this->myencryption->decode($id)));
      return $this->db->affected_rows();
    }
	function pic($id){
      $this->db->select('potret')
      ->where('id_berita',$this->myencryption->decode($id));
      return $this->db->get('berita')->row();
    }
	
    function update_blog($data1, $id){
      $ids = $this->myencryption->decode($id);
	  $this->db->where('id_berita', $ids);
      $this->db->update('berita', $data1);
      return $this->db->affected_rows();
    }/*
    function add_pasien($data){
      $this->db->insert('data_pasien', $data);
      return $this->db->affected_rows();
    }*/
	function edit_blog($id){
      $this->db->select('*')
      ->where('id_berita',$this->myencryption->decode($id));
      return $this->db->get('berita')->row();
    }
	
	function add_blog($data){
      $this->db->insert('berita', $data);
      return $this->db->affected_rows();
    }
	}
?>
