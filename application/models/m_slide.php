<?php
class M_slide extends CI_Model {
    // table name
    function __construct()
    {
        parent::__construct();
    }
    function get_pic(){
      $this->db->select('*');
	   $this->db->order_by("tanggal","desc");
      $tampung=$this->db->get('slide_picture')->result();
	  if($tampung){
      foreach($tampung as $temp):
      $temp1[]=array("picture"=>$temp->picture,
                     "tanggal"=>$temp->tanggal,
                     "idEn"=>$this->myencryption->encode($temp->id_pic));
      endforeach;

      return json_decode(json_encode($temp1),false);
	   }
    }
	
    function delete($id){
      $this->db->delete("slide_picture", array("id_pic"=>$this->myencryption->decode($id)));
      return $this->db->affected_rows();
    }
	function pic($id){
      $this->db->select('picture')
      ->where('id_pic',$this->myencryption->decode($id));
      return $this->db->get('slide_picture')->row();
    }
	/*
    function update_blog($data1, $id){
      $ids = $this->myencryption->decode($id);
	  $this->db->where('id_berita', $ids);
      $this->db->update('berita', $data1);
      return $this->db->affected_rows();
    }/*
    function add_pasien($data){
      $this->db->insert('data_pasien', $data);
      return $this->db->affected_rows();
    }
	function edit_blog($id){
      $this->db->select('*')
      ->where('id_berita',$this->myencryption->decode($id));
      return $this->db->get('berita')->row();
    }*/
	
	function add_slide($data){
      $this->db->insert('slide_picture', $data);
      return $this->db->affected_rows();
    }
	}
?>
