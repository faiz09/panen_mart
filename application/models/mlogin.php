<?php
class Mlogin extends CI_Model {
    // table name
    function __construct()
    {
        parent::__construct();
    }

	function get_user($username)
	{
		$this->db->where('userAccount',$username);
			//->where('userPassword',md5($password));
		return $this->db->get('adm_user')->row();
	}
	
	function get_user_agent($username, $password)
	{
		$this->db->where('member_acount',$username)
			->where('level','agent');
			//->where('member_pass',md5($password));
		return $this->db->get('member')->row();
	}
	
	function save_session($session,$username,$jenis)
	{
	$data = array('loginSession'=>$session, 'loginUser'=>$username, 'loginGroup'=>$jenis,'loginWaktu'=>date('Y-m-d H:i:s'));
	$this->db->insert('stlogin',$data);
	return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
	}
	
	function is_login($session,$user,$group)
	{
		$this->db->where('loginUser',$user)
			->where('loginSession',$session)
			->where('loginGroup',$group);
		$res = $this->db->get('stlogin')->num_rows();
		//return $res > 0 ? TRUE : FALSE;
		if($res > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}

  function cek_user($cek){
		$this->db->where(array("userAccount"=>$cek));
		$nm = $this->db->get('adm_user')->result();
		if(!empty($nm)) {
			$nm2 = True;
		}else {
			$nm2 = False;
		}
		return $nm2;
  }
  
  function cek_user_agent($cek){
		$this->db->where(array("member_acount"=>$cek, "level"=>'agent'));
		$nm = $this->db->get('member')->result();
		if(!empty($nm)) {
			$nm2 = True;
		}else {
			$nm2 = False;
		}
		return $nm2;
  }
  
	function get_user_nama($user)
	{
		$this->db->where('userAccount',$user);
		$user = $this->db->get('adm_user')->row();
		$r = 'Admin / '.$user->userNama;
		return $r;
	}
	
	function get_user_nama_agent($user)
	{
		$this->db->where(array("member_acount"=>$user, "level"=>'agent'));
		$user = $this->db->get('member')->row();
		$r = 'Agent / '.$user->nama;
		return $r;
	}
	
	function get_id_agent($user)
	{
		$this->db->where(array("member_acount"=>$user, "level"=>'agent'));
		$user = $this->db->get('member')->row();
		$r = $user->id_member;
		return $r;
	}
	
	function get_id_admin($user)
	{
		$this->db->where("userAccount",$user);
		$user = $this->db->get('adm_user')->row();
		$r = $user->id_admin;
		return $r;
	}
}
?>
