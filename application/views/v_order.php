<!-- <h1 class="page-title">Welcome Admin</h1>
 DRAG/DROP COLUMNS REORDER -->
<div class="panel">
	<div class="panel-heading">
		<h3 class="panel-title">List Order</h3>
	</div>
	<div class="panel-body">
		<ul class="nav nav-tabs" role="tablist">
			<li class="active"><a href="#lst" role="tab" data-toggle="tab">List Order</a></li>
			<li><a href="#lst1" role="tab" data-toggle="tab">List Report Order</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane fade in active" id="lst">
				
				<table id="featured-datatable" class="table table-hover table-bordered">
					<thead>
						<tr>
							<th>No</th>
							<th>Kode Transaksi</th>
							<th>Customer Name</th>
							<th>Agent Name</th>
							<th>Total Product</th>
							<th>Total Cost + Ongkir</th>
							<th>Payment Mathod</th>
							<th>Actios</th>
						</tr>
					</thead>
					<tbody>
					
					<?php $no = 1;
					   if(!empty($order)){
					   foreach($order as $ord):
						$cart = explode(',',$ord->cart);
						$jumlah_barang = count($cart);
						
						if($ord->metode == '1'){
							$metode = 'Transfer';
						}else{
							$metode = 'Cash On Delivery';
						}
						
						if($ord->status == '3'){
							$com = 'selected';
							$pro = '';
						}else{
							$com = '';
							$pro = 'selected';
						}
						  echo "
							<tr>
								<td>$no</td>
								<td>$ord->kode</td>
								<td>$ord->member</td>
								<td>$ord->agent</td>
								<td>$jumlah_barang</td>
								<td>Rp. $ord->total_harga + Rp. $ord->kirim</td>
								<td>$metode</td>
								<td>
								<a href='".site_url('order/detail/'.$ord->idEn)."'><button type='button' class='btn btn-info'><i class='fa fa-info-circle'></i> <span>View Detail</span></button></a>
								</br>
								<select name='single_selection[]' dataq='".$ord->idEn."' class='multiselect multiselect-custom single-selection status' onchange='chg_stat(this)'>
									<option value='processing' $pro ><i class='fa fa-refresh'></i> Processing</option>
									<option value='completed' $com ><i class='fa fa-check-circle'></i> Completed</option>
								</select>
								</td>
							</tr>";
							$no++;
						  endforeach;  }?>
					  
					</tbody>
				</table>
			</div>
			<div class="tab-pane fade" id="lst1">
				<table id="datatable-data-export" class="table table-striped table-hover table-bordered">
					<thead>
						<tr>
							<th>No</th>
							<th>Kode Transaksi</th>
							<th>Customer Name</th>
							<th>Agent Name</th>
							<th>Total Product</th>
							<th>Total Cost + Ongkir</th>
							<th>Payment Mathod</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
					
					<?php $no = 1;
					   if(!empty($order)){
					   foreach($order as $ord):
						$cart = explode(',',$ord->cart);
						$jumlah_barang = count($cart);
						
						if($ord->metode == '1'){
							$metode = 'Transfer';
						}else{
							$metode = 'Cash On Delivery';
						}
						
						if($ord->status == '3'){
							$stat = 'Order sudah selesai';
						}else if($ord->status == '2'){
							$stat = 'Order belum selesai';
						}else if($ord->status == '1' || $ord->status == '0'){
							$stat = 'Order belum belesai dan struk belum di upload';
						}
						  echo "
							<tr>
								<td>$no</td>
								<td>$ord->kode</td>
								<td>$ord->member</td>
								<td>$ord->agent</td>
								<td>$jumlah_barang</td>
								<td>Rp. $ord->total_harga + Rp. $ord->kirim</td>
								<td>$metode</td>
								<td>$stat</td>
							</tr>";
							$no++;
						  endforeach;  }?>
					  
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	$(".menus").removeClass("active");
	$("#order").addClass("active");

});

function chg_stat(stat)
{
	var id = $(stat).attr("dataq");
	var val = $(stat).val();
	//alert('ini '+id+' val '+val);
	location.href = "<?php echo site_url('order/change_to/'); ?>"+id+"/"+val; 
}
</script>