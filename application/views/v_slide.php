<div class="panel">
	<div class="panel-heading">
		<h2 class="panel-title">List Slider</h2>
	</div>
	<div class="panel-body">
		<p class="text-right"><a class='btn btn-primary btn-xs' href="<?php echo site_url('slide/tambah_picture');?>"><i class='fa fa-plus'>&nbsp;Tambah Gambar</i></a></p>
		<p class="alert alert-info deleted_s hidden"><i class="fa fa-info-circle"></i> Data berhasil di hapus.</p>
		<table id="featured-datatable" class="table table-striped table-hover">
			<thead>
				<tr>
					<th class="col-sm-1">No.</th>
					<th class="col-sm-2">Gambar</th>
					<th class="col-sm-1">Tanggal</th>
					<th class="col-sm-2">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php $no = 1;
					if(!empty($pic)){
					   foreach($pic as $picture):

						  echo "
                            <tr>
								<td>$no</td>
								<td>$picture->picture</td>
                                <td>$picture->tanggal</td>
                                <td>
								<!--<div class='btn-group'>-->
								<a class='btn btn-danger btn-xs' href='#' onclick='hapus(\"$picture->picture\",\"$picture->idEn\")'><span class='sr-only'>Remove</span><i class='fa fa-remove'>&nbsp;Remove</i></a>
								<!--<a class='btn btn-success btn-xs' href='".site_url('blog/edit/'.$picture->idEn)."'><span class='sr-only'>Ok</span><i class='fa fa-info'></i></a>-->
								<!--</div>--->
								
                            </tr>";
							$no++;
						endforeach; }?>
						  
			</tbody>
		</table>
		
	<!--<div id="large-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
					<h4 class="modal-title" id="myModalLabel">Large Modal Title</h4>
				</div>
				<div class="modal-body">
					<p>Modal dialog content...</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
					<button type="button" class="btn btn-primary"><i class="fa fa-check-circle"></i> Save changes</button>
				</div>
			</div>
		</div>
	</div>-->
	
</div>
</div>
<script>
$(document).ready(function(){
	$(".menus").removeClass("active");
	$("#slide").addClass("active");
});

function hapus(data1, id){
swal({
title:"Hapus Gambar : "+data1,
text:"Yakin akan menghapus data ini?",
type: "warning",
showCancelButton: true,
confirmButtonText: "Hapus",
closeOnConfirm: true,
},
function(){
 $.ajax({
                            url: "<?php echo site_url('slide/delete/'); ?>/"+id,
                            type: "post",
                            dataType:"json",
                           data:{ id: id },
                            success: function(d) {
   							toastr.success('Data Berhasil Dihapus.', {timeOut: 5000});
							 $("#featured-datatable").load("<?php echo site_url('slide'); ?> #featured-datatable");
							//$('#isi_p').html("Data Berhasil Di Hapus..!");
							//$('#myModalLabel2').html("Succsess");
							//$('.deleted_s').modal('show');
							// setTimeout(function(){
							//	 $('#myModal').modal('hide');
							// }, 2000);
							 
						   //setTimeout(function() {
							//	window.location.href = "<?php echo site_url('main'); ?>";
							//}, 2500);
							
                           },
                          error: function(){
    	   				  toastr.error('Data Gagal Dihapus.', {timeOut: 5000});
						   $("#featured-datatable").load("<?php echo site_url('slide'); ?> #featured-datatable");
                            //$('#isi_p').html("Terjadi kesalahan yang tidak diinginkan...");
							//$('#myModalLabel2').html("Fail");
							// $('#myModal').modal('show');
							// setTimeout(function(){
							//	 $('#myModal').modal('hide');
							 //}, 1500);
                             }
                            })
                   //batas ajax
  });
}
</script>