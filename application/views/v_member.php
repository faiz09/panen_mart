<!-- <h1 class="page-title">Welcome Admin</h1>
 DRAG/DROP COLUMNS REORDER -->
<div class="panel">
	<div class="panel-heading">
		<h3 class="panel-title">List Member</h3>
	</div>
	<div class="panel-body">
		<!--<p class="text-right"><a class='btn btn-primary btn-xs' href="<?php echo site_url('member/tambah_member');?>"><i class='fa fa-plus'>&nbsp;Tambah Member</i></a></p>-->
		<!--<p class="alert alert-info"><i class="fa fa-info-circle"></i> Try drag and drop the column to another position to reorder table columns.</p>-->
		<table id="featured-datatable" class="table table-hover table-bordered">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>Jenis Kelamin</th>
					<th>Telepon</th>
					<th>Email</th>
					<th>Alamat</th>
					<th>Level</th>
					<th>Status</th>
					<th>Actios</th>
				</tr>
			</thead>
			<tbody>
				<?php $no = 1;
					if(!empty($member)){
					   foreach($member as $mmbr):
						  $stat = $mmbr->status;
						  $lvl = $mmbr->level;
			
						  if($stat == 1){
							$p = 'Aktif';
						  }else if($stat == 0){
							$p = 'Tdk Aktif';
						  }else if($stat == 2){
							$p = 'Blokir';
						  }
							
						  echo "
                            <tr>
								<td>$no</td>
								<td>$mmbr->nama</td>
								<td>$mmbr->kelamin</td>
								<td>$mmbr->telepon</td>
								<td>$mmbr->email</td>
								<td>$mmbr->alamat</td>
								<td>".ucfirst($mmbr->level)."</td>
								<td>".$p."</td>
                                <td>
								<a href='".site_url('member/edit/'.$mmbr->idEn)."'><button type='button' class='btn btn-info'><i class='fa fa-info-circle'></i> <span>Edit</span></button></a>
								<button type='button' class='btn btn-warning' onclick='hapus(\"$mmbr->nama\",\"$mmbr->idEn\")'><i class='fa fa-trash-o'></i></button>
								</td>
								
                            </tr>";
							$no++;
						endforeach; }?>
				
			</tbody>
		</table>
	</div>
</div>
<script>
$(document).ready(function(){
	$(".menus").removeClass("active");
	$("#member").addClass("active");
});

/*function deactive(ids)
{
	//alert('ini '+data1+' di '+stat);
	location.href = "<?php echo site_url('product/deactive/'); ?>"+ids; 
}*/

function hapus(data1, id){
swal({
title:"Hapus Member : "+data1,
text:"Yakin akan menghapus data ini?",
type: "warning",
showCancelButton: true,
confirmButtonText: "Hapus",
closeOnConfirm: true,
},
function(){
 $.ajax({
                            url: "<?php echo site_url('member/delete/'); ?>/"+id,
                            type: "post",
                            dataType:"json",
                           data:{ id: id },
                            success: function(d) {
   							toastr.success('Data Berhasil Dihapus.', {timeOut: 5000});
							 $("#featured-datatable").load("<?php echo site_url('product'); ?> #featured-datatable");
							
                           },
                          error: function(){
    	   				  toastr.error('Data Gagal Dihapus.', {timeOut: 5000});
						   $("#featured-datatable").load("<?php echo site_url('product'); ?> #featured-datatable");
                            
                             }
                            })
                   //batas ajax
  });
}
</script>