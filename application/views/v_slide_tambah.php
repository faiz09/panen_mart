<!--<h1 class="page-title"></h1>-->
<div class="row">
<div class="col-md-12">
	<div class="panel">
		<div class="panel-heading">
			<h3 class="panel-title">Tambah Gambar Slide</h3>
		</div>
		<div class="panel-body">
		<div class="form-horizontal">
			<form id="form-tambah_blog" class="col-md-10" data-parsley-validate novalidate role="form" method="POST" enctype="multipart/form-data">
				
				<div class="form-group">
					<div class="col-md-10">
						<h4>Masukkan gambar. Hanya format jpg, jpge, & png yang diperbolehkan dengan ukuran di bawah 500 Kb</h4>
						<input type="file" name="file" class="dropify" data-allowed-file-extensions="jpg png jpeg" data-max-file-size="500K" data-height="300">
						<?php echo form_error('file', '<p class="text-danger">'); ?>
						<input type="hidden" id="file_v" name="file_v" value="" >
						<div id="filec">
						<?php echo form_error('file_v', '<p class="text-danger">'); ?>
						</div>
					</div>
				</div>
					<!--<div class="col-md-6">
						<h4>With event and default file, try to remove the image</h4>
						<input type="file" id="dropify-event" data-default-file="assets/img/login-bg.jpg">
					</div>-->
					
				<button type="submit" class="btn sub btn-primary" >Tambahkan</button>
				</div>
			</form>
		</div>
		</div>
	</div>
</div>
</div>
<script src="<?php echo base_url(); ?>component/theme/assets/vendor/dropify/js/dropify.min.js"></script>
<script>
$(document).ready(function(){
	$(".menus").removeClass("active");
	$("#slide").addClass("active");
});	

	var drEvent = $('.dropify').dropify();
	/*var sub = $('#sub');
	
	drEvent.on('change', function(event, element){
		//alert('Has Errors!');
		sub.disabled = false;
		$('.sub').removeClass('btn-danger');
		$('.sub').addClass('btn-primary');
	});
	
	drEvent.on('dropify.errors', function(event, element){
		//alert('Has Errors!');
		sub.disabled = true;
		$('.sub').removeClass('btn-primary');
		$('.sub').addClass('btn-danger');
	});
	
	$(function(){
	});
	
	$('#pic').bind('change', function(){
		var a =0;
		var error = "";
		var ext = $('#pic').val().split('.').pop().toLowerCase();
		if ($.inArray(ext, ['png','jpg','jpeg']) == -1){
			error = error+"Ekstensi File Bukan .png, .jpg & .jpeg";
		}else{
			a = a+1;
		}
		var picsize = (this.files[0].size);
		if (picsize > 500000){
			if(a == 1){
				error = error+"Ukuran File Melebihi 500 Kb.";
			}else{
				error = error+" Serta Ukuran File Melebihi 500 Kb.";
			}
		}else{
			a = a+1;
		}
		document.getElementById('file_v').value = error;
		document.getElementById('filec').innerHTML = "";
		});*/
	</script>