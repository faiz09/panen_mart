<!-- <h1 class="page-title">Welcome Admin</h1>
 DRAG/DROP COLUMNS REORDER -->
<div class="panel">
	<div class="panel-heading">
		<h3 class="panel-title">List Stok</h3>
	</div>
	<div class="panel-body">
		<!--<p class="alert alert-info"><i class="fa fa-info-circle"></i> Try drag and drop the column to another position to reorder table columns.</p>-->
		<table id="featured-datatable" class="table table-hover table-bordered">
			<thead>
				<tr>
					<th>No</th>
					<th>Kategori</th>
					<th>Produk</th>
					<th>Agent</th>
					<th>Harga Beli</th>
					<th>Harga Jual</th>
					<th>Jumlah</th>
					<th>Tanggal Stok Masuk</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
			<?php $no = 1;
			if(!empty($stok)){
			   foreach($stok as $stock):
				  $stat = $stock->status;
	
				  if($stat == 1){
					$p = '<span class="lnr lnr-arrow-up-circle">&nbsp;Ada</span>';
				  }else if($stat == 2){
					$p = '<span class="lnr lnr-arrow-right-circle">&nbsp;Habis</span>';
				  }else if($stat == 3){
					$p = '<span class="lnr lnr-arrow-right-circle">&nbsp;Dikembalikan</span>';
				  }
				
				  echo "
					<tr>
						<td>$no</td>
						<td>$stock->category</td>
						<td>$stock->product</td>
						<td>$stock->agent</td>
						<td>$stock->harga_beli</td>
						<td>$stock->harga_jual</td>
						<td>$stock->jumlah $stock->satuan</td>
						<td>$stock->tanggal</td>
						<td>".$p."</td>
						
					</tr>";
					$no++;
				endforeach; }?>
				
			</tbody>
		</table>
	</div>
</div>
<script>
$(document).ready(function(){
	$(".menus").removeClass("active");
	$("#stok").addClass("active");
});
</script>