  <header id="header" class="header-v3 header-v2">
			<div class="header-top">
                <div class="container container-ver2">
                    <div class="box">
                        <p class="icon-menu-mobile"><i class="fa fa-bars" ></i></p>
                        <div class="logo"><a href="#" title="Panen Mart">
                            <img src="<?php echo base_url();?>component/img/logo-typo2.png" alt="images">
                        </a></div>
                        <div class="logo-mobile"><a href="#" title="Panenmart"><img src="<?php echo base_url();?>component/img/logo-typo2.png" alt="Panenmart"></a></div>
						<nav class="mega-menu float-left">
                        <!-- Brand and toggle get grouped for better mobile display -->
                          <ul class="nav navbar-nav" id="navbar">
							  <li class="level1">&nbsp;</li>
							  <li class="level1"><a href="<?php echo site_url('main/index');?>" title="Beranda">Beranda</a></li>
                              <li class="level1"><a href="<?php echo site_url('main/about_us');?>" title="Tentang Kami">Tentang Kami</a></li>
                          </ul>
                        </nav>
                    </div>
                </div>
                <!-- End container -->
            </div>
            <!-- End header-top -->
        </header><!-- /header -->
		</br>
        <div class="container">
            <div class="banner-header banner-lbook3 space-30">
                <img src="<?php echo base_url();?>component/img/header.png" alt="Banner-header">
                <!--<div class="text">
                        <h3>About Us</h3>
                        <p><a href="<?php echo site_url('main/index');?>" title="Home">Home</a><i class="fa fa-caret-right"></i>About Us</p>
                    </div>-->
            </div>
        </div>
        <div class="page-about">
            <div class="container container-ver2 space-50 head-about">
                <div class="choose-us">
                    <div class="title-choose align-center about">
                        <!--<h4>Welcome to PanenMart</h4>-->
                        <h3>CERITA PANENMART</h3>
                        <div class="align-center border-choose">
                            <div class="images">
                                <img src="<?php echo base_url();?>component/assets/images/bg-border-center.png" alt="icon">
                            </div>
                            <!--End images-->
                        </div>
                        <!--End border-->
                    </div>
                </div>
                <!--End choose-us-->
                <div class="row">
                    <div class="col-md-6">
                        <img class="img-responsive" src="<?php echo base_url();?>component/img/tentang_kami.png" alt="images">
                    </div>
                    <!--End col-md-6-->
                    <div class="col-md-6 pd-left-10">
                        <h3>AWAL MULA PANENMART</h3>
                        <!--<p>Pada awal tahun 2016 harga sejumlah pangan mengalami lonjakan yang salah satu penyebab tingginya harga pangan ini adalah, rantai pasokan yang terlalu panjang. Faktanya terlihat harga pangan tetap rendah di tingkat petani, namun melambung tinggi di tingkat konsumen. Melihat permasalahan yang terjadi, dibutuhkan pembenahan terhadap rantai pasokan pangan yang ada saat ini.</p>-->
                        <p>Pertumbuhan pengguna internet yang sedemikian pesatnya merupakan suatu kenyataan yang membuat internet menjadi salah satu media yang efektif bagi perusahaan maupun perseorangan untuk memperkenalkan dan menjual barang barang dan jasa kepada konsumen. Perdagangan secara elektronik (e-Commerce) tidak terlepas dari laju pertumbuhan internet karena e-Commerce berjalan melalui jaringan internet, dan dapat dikatakan e-Commerce menjadi penggerak ekonomi baru dalam bidang teknologi.</p>
                        <p>Didirikan pada tanggal 21 Juni 2016 bertepatan dengan hari Krida Tani Indonesia, Panenmart yang berasal dari kata panen yang berarti pemungutan atau pemetikan dari hasil bercocok tanam, sedangkan mart atau market yang berarti pasar. Panenmart diharapkan nantinya dapat menjadi wadah bagi petani untuk terhubung dengan konsumen akhir dan bisa memasarkan hasil panennya langsung ke konsumen akhir. Melalui aplikasi berbasis mobile, Panenmart berupaya memberikan solusi perdagangan produk pangan secara online agar tercapai stabilitas harga pangan dengan memperpendek rantai distribusi dari petani hingga ke konsumen akhir dan membantu petani untuk meningkatkan akses mereka terhadap pasar agar petani sebagai produsen menjadi posisi yang lebih adil serta konsumen dapat menikmati pangan yang segar langsung dari petani.</p>
						<!--<div class="slider-about owl-carousel">
                            <div class="items">
                                <a href="#" title="post-item">
                                    <img src="<?php echo base_url();?>component/assets/images/about-blog-1.jpg" alt="images">
                                </a>
                            </div>
                            <!--End items-->
                            <!--<div class="items">
                                <a href="#" title="post-item">
                                    <img src="<?php echo base_url();?>component/assets/images/about-blog-1.jpg" alt="images">
                                </a>
                            </div>
                            <!--End items-->
                            <!--<div class="items">
                                <a href="#" title="post-item">
                                    <img src="<?php echo base_url();?>component/assets/images/about-blog-1.jpg" alt="images">
                                </a>
                            </div>
                            <!--End items-->
                        <!--</div>-->
                        <!--End slider-->
                    </div>
                    <!--End col-md-6-->
                </div>
                <!--End row-->
            </div>
            <!--End container-->
			<br><br>
			<br><br>
            <div class="our-team">
                <div class="container container-ver2 center">
                    <div class="choose-us">
                        <div class="title-choose align-center about">
                            <!--<h4>We Are Family</h4>-->
                            <h3>TIM PANENMART</h3>
                            <div class="align-center border-choose">
                                <div class="images">
                                    <img src="<?php echo base_url();?>component/assets/images/bg-border-center.png" alt="icon">
                                </div>
                                <!--End images-->
                            </div>
                            <!--End border-->
                        </div>
                    </div>
                    <!--End choose-us-->
					<div class="row">
						<div class="col-md-4 col-sm-4 space-30">
						</div>
						<div class="col-md-4 col-sm-4 space-30">
                            <div class="interactive-banner interactive-banner-v1 center">
                                <div class="interactive-banner-body">
                                    <img class="img-responsive" src="<?php echo base_url();?>component/img/owner/rsz_comission3.png" alt="Commissioner">
                                </div> 
                            </div>                                                                           
                            <div class="banner-title">
                                <h3>FAISAL SUHAELI</h3>
                                <p>Commissioner</p>
                            </div>
                        </div>
						<!-- End col-md-3 -->
					</div>
                    <div class="row">
                        <div class="col-md-4 col-sm-4 space-30">
                            <div class="interactive-banner interactive-banner-v1 center">
                                <div class="interactive-banner-body">
                                    <img class="img-responsive" src="<?php echo base_url();?>component/img/owner/ceo2.png" alt="CEO">
                                </div> 
                            </div>                                                                           
                            <div class="banner-title">
                                <h3>YAFSHIL ADIPURA</h3>
                                <p>Chief Executive Officer</p>
                            </div>
                        </div>
                        <!-- End col-md-3 -->
                        <div class="col-md-4 col-sm-4 space-30">
                            <div class="interactive-banner interactive-banner-v1 center">
                                <div class="interactive-banner-body">
                                    <img class="img-responsive" src="<?php echo base_url();?>component/img/owner/cmo2.png" alt="CMO">
                                </div> 
                            </div>                                                                           
                            <div class="banner-title">
                                <h3>ACHMAD ROSYADI</h3>
                                <p>Chief Financial Officer</p>
                            </div>
                        </div>
                        <!-- End col-md-3 -->
                        <div class="col-md-4 col-sm-4 space-30">
                            <div class="interactive-banner interactive-banner-v1 center">
                                <div class="interactive-banner-body">
                                    <img class="img-responsive" src="<?php echo base_url();?>component/img/owner/cto2.png" alt="CTO">
                                </div> 
                            </div>                                                                           
                            <div class="banner-title">
                                <h3>AKMAL CHAER</h3>
                                <p>Chief Technology Officer</p>
                            </div>
                        </div>
                        <!-- End col-md-3 -->
                    </div>
					</br></br>
                    <!-- End row -->
                </div>
                <!-- End container-ver2 -->
            </div>
            <!-- End our team -->
        </div>