<!-- <h1 class="page-title">Welcome Admin</h1>
 DRAG/DROP COLUMNS REORDER -->
<div class="panel">
	<div class="panel-heading">
		<h3 class="panel-title">List Category</h3>
	</div>
	<div class="panel-body">
		<p class="text-right"><a class='btn btn-primary btn-xs' href="<?php echo site_url('category/tambah_category');?>"><i class='fa fa-plus'>&nbsp;Tambah Category</i></a></p>
		<!--<p class="alert alert-info"><i class="fa fa-info-circle"></i> Try drag and drop the column to another position to reorder table columns.</p>-->
		<table id="featured-datatable" class="table table-hover table-bordered">
			<thead>
				<tr>
					<th>No</th>
					<th>Urutan</th>
					<th>Catagory Name</th>
					<th>Url Slug</th>
					<th>Gambar</th>
					<th>Status</th>
					<th>Actios</th>
				</tr>
			</thead>
			<tbody>
				
				<?php $no = 1;
					if(!empty($category)){
					  $last = count($category);
					   foreach($category as $kategori):
						  $stat = $kategori->status;
			
						  if($stat == 1){
							$p = '<span class="lnr lnr-arrow-up-circle">&nbsp;Aktif</span>';
						  }else if($stat == 0){
							$p = '<span class="lnr lnr-arrow-right-circle">&nbsp;Tdk Aktif</span>';
						  }
						  
						  if($no == 1){
							$q = "<button type='button' class='btn btn-success btn-xs' onclick='swap(\"$no\",\"turun\")'><i class='fa fa-angle-double-down'></i></button>";
						  }else if($no == $last){
							$q = "<button type='button' class='btn btn-success btn-xs' onclick='swap(\"$no\",\"naik\")'><i class='fa fa-angle-double-up'></i></button>";
						  }else{
							$q = "<button type='button' class='btn btn-success btn-xs' onclick='swap(\"$no\",\"naik\")'><i class='fa fa-angle-double-up'></i></button>
								<button type='button' class='btn btn-success btn-xs' onclick='swap(\"$no\",\"turun\")'><i class='fa fa-angle-double-down'></i></button>";
						  }
							
						  echo "
                            <tr>
								<td>$no</td>
								<td>
								".$q."
								</td>
                                <td>$kategori->nama</td>
                                <td>$kategori->slug</td>
                                <td>$kategori->gambar</td>
								<td>".$p."</td>
                                <td>
								<div class='btn-group'>
								<a href='".site_url('category/edit/'.$kategori->idEn)."'><button type='button' class='btn btn-info'><i class='fa fa-info-circle'></i> <span>Edit</span></button></a>
								<button type='button' class='btn btn-warning' onclick='hapus(\"$kategori->nama\",\"$kategori->idEn\")'><i class='fa fa-trash-o'></i></button>
								</div>
								
                            </tr>";
							$no++;
                          endforeach; }?>
				
				<!--<tr>
					<td>1</td>
					<td>
					<button type="button" class="btn btn-success btn-xs"><i class="fa fa-angle-double-up"></i></button>
					<button type="button" class="btn btn-success btn-xs"><i class="fa fa-angle-double-down"></i></button>
					</td>
					<td>Nuts</td>
					<td>nuts</td>
					<td><button type="button" class="btn btn-info"><i class="fa fa-info-circle"></i> <span>Edit</span></button>
					<button type="button" class="btn btn-warning"><i class="fa fa-trash-o"></i></button>
					</td>
				</tr>-->
			</tbody>
		</table>
	</div>
</div>
<script>
$(document).ready(function(){
	$(".menus").removeClass("active");
	$("#category").addClass("active");
});

function swap(data1,stat)
{
	//alert('ini '+data1+' di '+stat);
	location.href = "<?php echo site_url('category/swap/'); ?>"+data1+"/"+stat; 
}

function hapus(data1, id){
swal({
title:"Hapus Category : "+data1,
text:"Yakin akan menghapus data ini?",
type: "warning",
showCancelButton: true,
confirmButtonText: "Hapus",
closeOnConfirm: true,
},
function(){
 $.ajax({
                            url: "<?php echo site_url('category/delete/'); ?>/"+id,
                            type: "post",
                            dataType:"json",
                           data:{ id: id },
                            success: function(d) {
   							toastr.success('Data Berhasil Dihapus.', {timeOut: 5000});
							 $("#featured-datatable").load("<?php echo site_url('category'); ?> #featured-datatable");
							
                           },
                          error: function(){
    	   				  toastr.error('Data Gagal Dihapus.', {timeOut: 5000});
						   $("#featured-datatable").load("<?php echo site_url('category'); ?> #featured-datatable");
                            
                             }
                            })
                   //batas ajax
  });
}
</script>