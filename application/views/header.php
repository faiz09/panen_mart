<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>component/assets/css/bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>component/assets/css/style.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>component/assets/vendor/owl-slider.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>component/assets/vendor/settings.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>component/assets/vendor/slick.css"/>
        <link rel="shortcut icon" href="<?php echo base_url();?>component/assets/images/favicon.png" />
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,500,700,300' rel='stylesheet' type='text/css'>
        <script type="text/javascript" src="<?php echo base_url();?>component/assets/js/jquery-3.2.0.min.js"></script>
        <title>Panenmart</title>
    </head>
    <body>
    <div class="awe-page-loading">
         <div class="awe-loading-wrapper">
            <div class="awe-loading-icon">
               <img src="<?php echo base_url();?>component/img/logo.png" alt="Panen Mart">
            </div>
            <div class="progress">
               <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
         </div>
      </div> 
    <!-- End pushmenu -->
    <div class="wrappage">