</div>
			</div>		
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<footer>
			<div class="container-fluid">
				<p class="copyright">&copy; 2017 . All Rights Reserved.</p>
			</div>
		</footer>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<!--<script src="<?php echo base_url(); ?>component/ckeditor/ckeditor.js"></script>-->
	<script src="<?php echo base_url(); ?>component/theme/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>component/theme/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<?php echo base_url(); ?>component/theme/assets/vendor/datatables/js-main/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url(); ?>component/theme/assets/vendor/datatables/js-bootstrap/dataTables.bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>component/theme/assets/vendor/datatables-colreorder/dataTables.colReorder.js"></script>
	<!--<script src="<?php echo base_url(); ?>component/theme/assets/vendor/datatables-tabletools/js/dataTables.tableTools.js"></script>-->
	<script src="<?php echo base_url(); ?>component/theme/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
	<script src="<?php echo base_url(); ?>component/theme/assets/vendor/parsleyjs/js/parsley.min.js"></script>
	<script src="<?php echo base_url(); ?>component/theme/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>component/theme/assets/scripts/klorofilpro-common.js"></script>
	<script src="<?php echo base_url(); ?>component/theme/assets/vendor/toastr/toastr.min.js"></script>
	<script src="<?php echo base_url(); ?>component/theme/assets/vendor/summernote/summernote.min.js"></script>
	<!-- DEMO PANEL -->
	<!-- for demo purpose only, you should remove it on your project directory -->
	
	<!-- Teble Exsport Plugin -->
	<script src="https://cdn.datatables.net/buttons/1.4.0/js/dataTables.buttons.min.js"></script>
	<script src="//cdn.datatables.net/buttons/1.4.0/js/buttons.flash.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
	<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
	<script src="//cdn.datatables.net/buttons/1.4.0/js/buttons.html5.min.js"></script>
	<script src="//cdn.datatables.net/buttons/1.4.0/js/buttons.print.min.js"></script>
	<script type="text/javascript">
	
		$('.single-selection').multiselect({
			maxHeight: 300
		});
		
		$(document).ready(function(){
			$('#datatable-data-export').DataTable( {
				//dom: 'l<"clear">Bfrtip',
				dom: "<'clearfix'>" +
				"<'row'<'col-sm-6'l><'col-sm-6'f>r>" +
				"Btip",
				buttons: [
					'copy', 'csv', 'excel', 'pdf', 'print'
				]
			});
		});
	</script>
	<!-- END DEMO PANEL -->
	<script>
	$(function() {

		// datatable column with reorder extension
		$('#datatable-column-reorder').dataTable({
			pagingType: "full_numbers",
			sDom: "RC" +
				"t" +
				"<'row'<'col-sm-6'i><'col-sm-6'p>>",
			colReorder: true,
		});

		// datatable with column filter enabled
		var dtTable = $('#datatable-column-filter').DataTable({ // use DataTable, not dataTable
			sDom: // redefine sDom without lengthChange and default search box
				"t" +
				"<'row'<'col-sm-6'i><'col-sm-6'p>>"
		});

		$('#datatable-column-filter thead').append('<tr class="row-filter"><th></th><th></th><th></th><th></th><th></th></tr>');
		$('#datatable-column-filter thead .row-filter th').each(function() {
			$(this).html('<input type="text" class="form-control input-sm" placeholder="Search...">');
		});

		$('#datatable-column-filter .row-filter input').on('keyup change', function() {
			dtTable
				.column($(this).parent().index() + ':visible')
				.search(this.value)
				.draw();
		});

		// datatable with paging options and live search
		$('#featured-datatable').dataTable({
			sDom: "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
		});
		
		// datatable with export feature
		/*var exportTable = $('#datatable-data-export').DataTable({
			sDom: "T<'clearfix'>" +
				"<'row'<'col-sm-6'l><'col-sm-6'f>r>" +
				"t" +
				"<'row'<'col-sm-6'i><'col-sm-6'p>>",
			"tableTools": {
				"sSwfPath": "../component/theme/assets/vendor/datatables-tabletools/swf/copy_csv_xls_pdf.swf"
			}
		});*/

		// datatable with scrolling
		$('#datatable-basic-scrolling').dataTable({
			scrollY: "300px",
			scrollCollapse: true,
			paging: false
		});
		
		$('.summernote').summernote({
		  height: 300,
		  focus: true,
		  toolbar: [
			// [groupName, [list of button]]
			['style', ['bold', 'italic', 'underline', 'clear']],
			['font', ['strikethrough', 'superscript', 'subscript']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']]
		  ]
		});
		
	});
	
	 /*-----------------------------------/
	/*	DATE PICKER
	/*----------------------------------*/

	$('.inline-datepicker').datepicker({
		todayHighlight: true
	});
	
	</script>
</body>

</html>
