<h1 class="page-title">Tambah Stok</h1>
<div class="row">
<div class="col-md-12">
	<div class="panel">
		<div class="panel-heading">
			<!--<h3 class="panel-title"></h3>-->
		</div>
		<div class="panel-body">
		<div class="form-horizontal">
			<form id="form-tambah_blog" class="col-md-10" data-parsley-validate novalidate role="form" method="POST" enctype="multipart/form-data">
				<div class="form-group">
					<label class="col-md-3 control-label">Kategory :</label>
					<div class="col-md-9">
						<?php
							$style2='class="form-control" id="categoryq" required onChange="list_produk()"';
							echo form_dropdown("category",$category,'',$style2);
						?>
						<?php echo form_error('category', '<p class="text-danger">');?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Produk :</label>
					<div class="col-md-9">
						<?php
							$style='class="form-control" id="produk" required onChange="get_satuan()"';
							echo form_dropdown("produk",$produk,'',$style);
						?>
						<?php echo form_error('produk', '<p class="text-danger">');?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Agent :</label>
					<div class="col-md-9">
						<?php
							$style1='class="form-control" id="agent" required';
							echo form_dropdown("agent",$agent,'',$style1);
						?>
						<?php echo form_error('agent', '<p class="text-danger">');?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Harga Jual :</label>
					<div class="col-md-5">
						<input type="text" id="jual" name="jual" class="form-control" value="<?php echo set_value('jual')?>" required data-parsley-type="number">
						<?php echo form_error('jual', '<p class="text-danger">'); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Harga Beli :</label>
					<div class="col-md-5">
						<input type="text" id="beli" name="beli" class="form-control" value="<?php echo set_value('beli')?>" required data-parsley-type="number">
						<?php echo form_error('beli', '<p class="text-danger">'); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Jumlah :</label>
					<div class="col-md-5">
						<input type="text" id="jumlah" name="jumlah" class="form-control" value="<?php echo set_value('jumlah')?>" required data-parsley-type="number">
						<?php echo form_error('jumlah', '<p class="text-danger">'); ?>
					</div>
					<label class="col-md-1 control-label" id="satuan"></label>
				</div>
				<br/>
				<!--<button type="button" class="btn btn-primary" onclick='validateForm()'>Validate</button>-->
				<button type="submit" class="btn btn-primary">Tambahkan</button>
			</form>
		</div>
		</div>
	</div>
</div>
</div>
<script>
$(document).ready(function(){
	$(".menus").removeClass("active");
	$("#stok").addClass("active");
	$('#agent').val('<?php echo set_value('agent'); ?>').attr("selected", "selected");
	$('#produk').val('<?php echo set_value('produk'); ?>').attr("selected", "selected");
});

function list_produk()
 {
	 ne = $("#categoryq").val();
	 //alert(ne);
	 $.ajax({
		 url:"<?php echo base_url();?>index.php/stok/pilih_produk/"+ne+"",
		 success: function(response){
			$("#produk").html(response);
			//alert(response);
		 },
		 dataType:"html"
	 });
	 return false;
 }
 
 function get_satuan()
 {
	 ne = $("#produk").val();
	 //alert(ne);
	 $.ajax({
		 url:"<?php echo base_url();?>index.php/stok/get_satuan/"+ne+"",
		 success: function(response){
			$("#satuan").html(response);
			//alert(response);
		 },
		 dataType:"html"
	 });
	 return false;
 }
</script>