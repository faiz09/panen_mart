<!doctype html>
<html lang="en">

<head>
	<title>Admin Page</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>component/theme/assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>component/theme/assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>component/theme/assets/vendor/linearicons/style.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>component/theme/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css">
	<script src="<?php echo base_url(); ?>component/theme/assets/vendor/jquery/jquery.min.js"></script>
	<link rel="stylesheet" href="<?php echo base_url(); ?>component/theme/assets/vendor/parsleyjs/css/parsley.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>component/theme/assets/vendor/datatables/css-main/jquery.dataTables.min.css">
	
	<!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"/>-->
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.4.0/css/buttons.dataTables.min.css"/>
	
	<!--<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.15/datatables.min.js"></script>-->
	
	<link rel="stylesheet" href="<?php echo base_url(); ?>component/theme/assets/vendor/datatables/css-bootstrap/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>component/theme/assets/vendor/datatables-tabletools/css/dataTables.tableTools.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>component/theme/assets/vendor/summernote/summernote.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>component/theme/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">
	<link href="<?php echo base_url(); ?>component/sweetalert/sweetalert.css" rel="stylesheet">
	<script type="text/javascript" src="<?php echo base_url('component/sweetalert/sweetalert.min.js'); ?>"></script>
	<link rel="stylesheet" href="<?php echo base_url(); ?>component/theme/assets/vendor/toastr/toastr.min.css">
	<script src="<?php echo base_url(); ?>component/ckeditor/ckeditor.js"></script>
	<link rel="stylesheet" href="<?php echo base_url(); ?>component/theme/assets/vendor/dropify/css/dropify.min.css">
	
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>component/theme/assets/css/main.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyB3m9bT9o8gNY9PrZgHJqW34kgu9jC9juI"></script>
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>component/theme/assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>component/theme/assets/img/favicon.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="brand">
				<a href="<?php echo site_url('admin');?>"><img src="<?php echo base_url(); ?>component/img/logo-typo.png" alt="Klorofil Logo" class="img-responsive logo"></a>
			</div>
			<div class="container-fluid">
				<div id="tour-fullwidth" class="navbar-btn">
					<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
				</div>
				<div id="navbar-menu">
					<ul class="nav navbar-nav navbar-right">
						
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span><?php echo $user;?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
							<ul class="dropdown-menu">
								<li><a href="#" data-toggle="modal" data-target="#small-modal"><i class="lnr lnr-user"></i> <span>My Profile</span></a></li>
								<li><a href="<?php echo site_url('login/logout');?>"><i class="lnr lnr-exit"></i> <span>Logout</span></a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<div id="sidebar-nav" class="sidebar" style="background-color:#2B333E;">
			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">
						<li>
							<a class="menus" id="admin" href="<?php echo site_url('admin');?>"><i class="lnr lnr-home"></i> <span>Home</span></a>
						</li>
						<li>
							<a class="menus" id="blog" href="<?php echo site_url('blog');?>"><i class="lnr lnr-layers"></i> <span>Blog</span></a>
						</li>
						<li>
							<a class="menus" id="slide" href="<?php echo site_url('slide');?>"><i class="lnr lnr-layers"></i> <span>Slide</span></a>
						</li>
						<li>
							<a class="menus" id="order" href="<?php echo site_url('order');?>"><i class="lnr lnr lnr-map"></i> <span>Order</span></a>
						</li>
						<li>
							<a class="menus" id="product" href="<?php echo site_url('product');?>"><i class="lnr lnr-inbox"></i> <span>Product</span></a>
						</li>
						<li>
							<a class="menus" id="category" href="<?php echo site_url('category');?>"><i class="lnr lnr-inbox"></i> <span>Manage Category</span></a>
						</li>
						<li>
							<a class="menus" id="stok" href="<?php echo site_url('stok');?>"><i class="lnr lnr-sync"></i> <span>Stok</span></a>
						</li>
						<li>
							<a class="menus" id="member" href="<?php echo site_url('member');?>"><i class="lnr lnr-user"></i> <span>Member/Agent</span></a>
						</li>
						<li>
							<a class="menus" id="request" href="<?php echo site_url('request');?>"><i class="lnr lnr-text-format"></i> <span>Request Agent</span></a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!--<ul class="breadcrumb">
						<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
						<li><a href="#">Tables</a></li>
						<li class="active">Dynamic Tables</li>
					</ul>-->
					
				<!--<button type="button" class="btn btn-default" data-toggle="modal" data-target="#small-modal">Small modal</button>-->
				<div id="small-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
					<div class="modal-dialog modal-md" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								<h4 class="modal-title" id="myModalLabel2">Admin Profile</h4>
							</div>
							<div class="modal-body">
								<!--<p>Edit Admin Profile</p>
								</br>--></br>
								<div class="form-horizontal">
									<form id="form-profil" class="col-md-10" action="<?php echo site_url('admin');?>/edit_admin" data-parsley-validate novalidate role="form" method="POST" enctype="multipart/form-data">									
										
										<div class="form-group">
											<label class="col-md-6 control-label">Nama Admin :</label>
											<div class="col-md-6">
												<input type="text" id="nama" name="nama" class="form-control" value="<?php echo set_value('nama',  substr($user,8))?>" required>
												<?php echo form_error('nama', '<p class="text-danger">');?>
											</div>
										</div>
										<div class="form-group">
											<label class="fancy-checkbox col-md-6">
												<input type="checkbox" name="pass_change" id="pass_change" >
												<span>Ingin Mengganti Password ?</span>
											</label>
										</div>
										<div class="form-group hidden change_pass">
											<label class="col-md-6 control-label">Password Lama :</label>
											<div class="col-md-6">
												<input type="password" id="pass1" name="pass1" class="form-control" value="" >
												<?php echo form_error('pass1', '<p class="text-danger">'); ?>
											</div>
										</div>
										<div class="form-group hidden change_pass">
											<label class="col-md-6 control-label">Password Baru :</label>
											<div class="col-md-6">
												<input type="password" id="pass2" name="pass2" class="form-control" value="" >
												<?php echo form_error('pass2', '<p class="text-danger">'); ?>
											</div>
										</div>
										<div class="form-group hidden change_pass">
											<label class="col-md-6 control-label">Konfirmasi Password Baru :</label>
											<div class="col-md-6">
												<input type="password" id="pass3" name="pass3" class="form-control" value="" data-parsley-samakan="" >
												<?php echo form_error('pass3', '<p class="text-danger">'); ?>
											</div>
										</div>
									</form>
								</div>
								
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
								<button type="button" class="btn btn-primary" id="sbmt"><i class="fa fa-check-circle"></i> Save changes</button>
							</div>
						</div>
					</div>
				</div>

<script>
$(document).ready(function(){
		$('.modal-body').css('min-height', '150px');
		$("#pass_change").click(function(){
			var up_pass = $('input[name="pass_change"]:checked').length;
			if(up_pass > 0){
				$('.change_pass').removeClass('hidden');
				//$(".change_pass").fadeOut();
				$(".change_pass").fadeIn();
				$('.modal-body').css('min-height', '300px');
				//$('.change_pass').setAttr("required", "");
				//$(".change_pass").prop('required',true);
				//$('.change_pass').attr('required', true);
				$(".change_pass").attr("required","true");
			}else{
				$('.change_pass').addClass('hidden');
				$(".change_pass").fadeOut();
				$('.modal-body').css('min-height', '150px');
				$('.change_pass').removeAttr('required');
				//$('.change_pass').attr('required', false);
			}
		});
		
		$("#sbmt").click(function(){
			var up_pass = $('input[name="pass_change"]:checked').length;
			if(up_pass > 0){
				$("#form-profil").submit();
			}else{
				$("#form-profil").submit();
			}
		});
		
		 window.Parsley.addValidator('samakan', function (value){
            // if checkbox with name 'requirement' is checked, field is required
			var up_pass = $('input[name="pass_change"]:checked').length;
			var pas  = $('#pass2').val();
			if(up_pass > 0){
				if(pas != value){
					return false;
				}
			}
            return true;
        }, 32).addMessage('en', 'samakan', 'Konfirmasi Password Baru Tidak Sama !');
		
});

</script>
					