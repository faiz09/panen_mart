<!-- <h1 class="page-title">Welcome Admin</h1>
 DRAG/DROP COLUMNS REORDER -->
<div class="panel">
	<div class="panel-heading">
		<h3 class="panel-title">List Product</h3>
	</div>
	<div class="panel-body">
		<p class="text-right"><a class='btn btn-primary btn-xs' href="<?php echo site_url('product/tambah_product');?>"><i class='fa fa-plus'>&nbsp;Tambah Product</i></a></p>
		<!--<p class="alert alert-info"><i class="fa fa-info-circle"></i> Try drag and drop the column to another position to reorder table columns.</p>-->
		<table id="featured-datatable" class="table table-hover table-bordered">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama Produk</th>
					<th>Satuan</th>
					<th>Kategori</th>
					<th>Gambar</th>
					<th>Status</th>
					<th>Actios</th>
				</tr>
			</thead>
			<tbody>
				<?php $no = 1;
					if(!empty($produk)){
					   foreach($produk as $product):
						  $stat = $product->status;
			
						  if($stat == 1){
							$p = '<span class="lnr lnr-arrow-up-circle">&nbsp;Aktif</span>';
							$dis = '';
							$bt = 'btn-primary';
						  }else{
							$p = '<span class="lnr lnr-arrow-right-circle">&nbsp;Tdk Aktif</span>';
							$dis = 'disabled';
							$bt = 'btn-danger';
						  }
							
						  echo "
                            <tr>
								<td>$no</td>
								<td>$product->nama</td>
								<td>$product->satuan</td>
                                <td>$product->category</td>
								<td>$product->gambar</td>
								<td>".$p."</td>
                                <td>
								<a href='".site_url('product/edit/'.$product->idEn)."'><button type='button' class='btn btn-info'><i class='fa fa-info-circle'></i> <span>Edit</span></button></a>
								<button type='button' class='btn $bt' onclick='deactive(\"$product->idEn\")' $dis><i class='lnr lnr-cross-circle'></i> <span>Deactive</span></button>
								<button type='button' class='btn btn-warning' onclick='hapus(\"$product->nama\",\"$product->idEn\")'><i class='fa fa-trash-o'></i></button>
								</td>
								
                            </tr>";
							$no++;
						endforeach; }?>
				
				<!--<tr>
					<td>1</td>
					<td>Here Will Be Your Product Title</td>
					<td>Rp.50000</td>
					<td>Nuts</td>
					<td>Aktif</td>
					<td><button type="button" class="btn btn-info"><i class="fa fa-info-circle"></i> <span>Edit</span></button>
					<button type="button" class="btn btn-primary"><i class="lnr lnr-cross-circle"></i> <span>Deactive</span></button>
					<button type="button" class="btn btn-warning"><i class="fa fa-trash-o"></i></button>
					</td>
				</tr>-->		
			</tbody>
		</table>
	</div>
</div>
<script>
$(document).ready(function(){
	$(".menus").removeClass("active");
	$("#product").addClass("active");
});

function deactive(ids)
{
	//alert('ini '+data1+' di '+stat);
	location.href = "<?php echo site_url('product/deactive/'); ?>"+ids; 
}

function hapus(data1, id){
swal({
title:"Hapus Product : "+data1,
text:"Yakin akan menghapus data ini?",
type: "warning",
showCancelButton: true,
confirmButtonText: "Hapus",
closeOnConfirm: true,
},
function(){
 $.ajax({
                            url: "<?php echo site_url('product/delete/'); ?>/"+id,
                            type: "post",
                            dataType:"json",
                           data:{ id: id },
                            success: function(d) {
   							toastr.success('Data Berhasil Dihapus.', {timeOut: 5000});
							 $("#featured-datatable").load("<?php echo site_url('product'); ?> #featured-datatable");
							
                           },
                          error: function(){
    	   				  toastr.error('Data Gagal Dihapus.', {timeOut: 5000});
						   $("#featured-datatable").load("<?php echo site_url('product'); ?> #featured-datatable");
                            
                             }
                            })
                   //batas ajax
  });
}
</script>