<h1 class="page-title">Tambah Product</h1>
<div class="row">
<div class="col-md-12">
	<div class="panel">
		<div class="panel-heading">
			<!--<h3 class="panel-title"></h3>-->
		</div>
		<div class="panel-body">
		<div class="form-horizontal">
			<form id="form-tambah_blog" class="col-md-10" data-parsley-validate novalidate role="form" method="POST" enctype="multipart/form-data">
				<div class="form-group">
					<label class="col-md-3 control-label">Nama Produk :</label>
					<div class="col-md-9">
						<input type="text" id="nama" name="nama" class="form-control" value="<?php echo set_value('nama')?>" required>
						<?php echo form_error('nama', '<p class="text-danger">');?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Satuan :</label>
					<div class="col-md-9">
						<input type="text" id="satuan" name="satuan" class="form-control" value="<?php echo set_value('satuan')?>" required>
						<?php echo form_error('satuan', '<p class="text-danger">'); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Category :</label>
					<div class="col-md-9">
						<?php
							$style='class="form-control" id="category"';
							echo form_dropdown("category",$category,'',$style);
						?>
						<?php echo form_error('category', '<p class="text-danger">');?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Gambar :</label>
					<div class="col-md-9">
						<input type="file" id="file" name="file" >
						<!--<p class="help-block"><em>Example block-level help text here.</em></p>-->
						<p class="help-block text-danger"><em id="fupload_error"></em></p>
						<input type="hidden" id="file_v" name="file_v" >
						<div id="filec">
						<?php echo form_error('file', '<p class="text-danger">'); ?>
						<?php echo form_error('file_v', '<p class="text-danger">'); ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Status :</label>
					<div class="col-md-9">
						<label class="fancy-radio">
							<input type="radio" name="status" value="1" <?php echo  set_radio('status', '1'); ?> required data-parsley-errors-container="#error-radio">
							<span><i></i>Aktif</span>
						</label>
						<label class="fancy-radio">
							<input type="radio" name="status" value="0" <?php echo  set_radio('status', '2'); ?>>
							<span><i></i>Tdk Aktif</span>
						</label>
						<p id="error-radio"></p>
						<?php echo form_error('status', '<p class="text-danger">'); ?>
					</div>
				</div>
				<br/>
				<!--<button type="button" class="btn btn-primary" onclick='validateForm()'>Validate</button>-->
				<button type="submit" class="btn btn-primary">Tambahkan</button>
			</form>
		</div>
		</div>
	</div>
</div>
</div>
<script>
$(document).ready(function(){
	$(".menus").removeClass("active");
	$("#product").addClass("active");
});
	
	$('#file').bind('change', function() {
		var a =0;
		var error = "";
		var ext = $('#file').val().split('.').pop().toLowerCase();
		if ($.inArray(ext, ['png','jpg','jpeg']) == -1){
			error = error+"Ekstensi File Bukan .png, .jpg & .jpeg";
		}else{
			a = a+1;
		}
		var picsize = (this.files[0].size);
		if (picsize > 100000){
			if(a == 1){
				error = error+"Ukuran File Melebihi 100 Kb.";
			}else{
				error = error+" Serta Ukuran File Melebihi 100 Kb.";
			}
		}else{
			a = a+1;
		}

		if(a == 2){
			document.getElementById('fupload_error').innerHTML = "<p class='text-success'>Memenuhi Ukuran Dan Ekstensi Yang Ditetapkan</p>";
		}else{
			document.getElementById('fupload_error').innerHTML = "<p class='text-danger'>"+error+"</p>";
		}
		document.getElementById('file_v').value = error;
		document.getElementById('filec').innerHTML = "";
		});
	</script>