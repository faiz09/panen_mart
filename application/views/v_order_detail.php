<!-- <h1 class="page-title">Welcome Admin</h1>
 DRAG/DROP COLUMNS REORDER -->
<style>
#map-canvas {
  width: 500px;
  height: 400px;
}
</style>
<div class="panel">
	<div class="panel-heading">
		<h3 class="panel-title">List Detail Order</h3>
	</div>
	<div class="panel-body">
		<div class="col-md-12">
			<ul class="list-unstyled list-insights">
				<li>
					<div class="media">
						<div class="media-left media-middle">
							<i class="fa fa-users custom-bg-orange"></i>
						</div>
						<div class="media-body">
							<p>Member : <?=$detail[0]->member;?>, Kode Transaksi : <?=$detail[0]->kode_transaksi;?></p>
							<p>Agent : <?=$detail[0]->agent;?></p>
						</div>
					</div>
				</li>
				<li>
					<div class="media">
						<div class="media-left media-middle">
							<i class="fa fa fa-shopping-cart custom-bg-green"></i>
						</div>
						<div class="media-body">
						<?php
							$no = 1;
							//$cart = explode(',',$detail[0]->cart);
							//var_dump($cart);
							foreach($cartq as $barang):
								$harga = $barang['jumlah'] * $barang['harga'];
								$br = number_format($barang['harga'],2,",",".");
								$br2 = number_format($harga,2,",",".");
								echo "<p>$no. $barang[nama](Rp. $br) Sebanyak $barang[jumlah] $barang[satuan] : Rp. $br2</p>";
							$no++;
							endforeach;
								$total_harga = number_format($detail[0]->total_harga,2,",",".");
								$kirim = number_format($detail[0]->kirim,2,",",".");
								$jadi = number_format(($detail[0]->total_harga)+($detail[0]->kirim),2,",",".");
							?>	
							<hr>
							<p>Total Belanja Adalah Rp. <?=$total_harga;?> + Ongkir(Rp. <?=$kirim;?>) = Rp. <?=$jadi;?></p>
		
						</div>
					</div>
				</li>
				<li>
					<div class="media">
						<div class="media-left media-middle">
							<i class="fa fa fa-map-marker custom-bg-blue"></i>
						</div>
						<div class="media-body">
							<p>Alamat : <?=$detail[0]->alamat;?></p>
							<p>Kecamatan : <?=$detail[0]->kecamatan;?></p>
							<p>Kabupaten : <?=$detail[0]->kabupaten;?></p>
							<p>Provinsi : <?=$detail[0]->provinsi;?></p>
							<p id="not_found">Latitude dan Longitude tidak diset sebelumnya.</p>
							<div id="map-canvas"></div>
						</div>
					</div>
				</li>
				<li>
					<div class="media">
						<div class="media-left media-middle">
							<i class="fa fa-phone custom-bg-yellow"></i>
						</div>
						<div class="media-body">
							<p><?=$detail[0]->telepon;?></p>
						</div>
					</div>
				</li>
				<li>
					<div class="media">
						<div class="media-left media-middle">
							<i class="fa fa-clock-o custom-bg-purple"></i>
						</div>
						<div class="media-body">
							<p><?=$detail[0]->tanggal_iat;?></p>
						</div>
					</div>
				</li>
				
				<?php
					if($detail[0]->metode=='1'){
						$tls = 'Transfer';
						$gmbr = $detail[0]->struk;
						if($gmbr != ''){
							$gg = "<img class='' src='".base_url()."component/upload/struk/".$gmbr."' alt='Struk' max-height='320' max-width='320' >";
						}else{
							$gg = '<p>Struk belum ditambahkan</p>';
						}
					}else{
						$tls = 'Cash On Delivery';
						$gg = "";
					}
				?>
				<li>
					<div class="media">
						<div class="media-left media-middle">
							<i class="fa fa-file-text-o custom-bg-red"></i>
						</div>
						<div class="media-body">
							<p><?=$tls;?></p>
							<?=$gg;?>
						</div>
					</div>
				</li>
			</ul>
		</div>
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-6">
					<div class="widget widget-stat">
						<div class="media">
							<div class="media-left media-middle">
								<i class="fa fa-money icon-transparent-area custom-color-green"></i>
							</div>
							<?php
								if($detail[0]->status=='0' || $detail[0]->status=='1'){
									$status1 = 'Order belum selesai dan struk belum diupload';
								}else if($detail[0]->status=='2'){
									$status1 = 'Order belum selesai';
								}else if($detail[0]->status=='3'){
									$status1 = 'Order sudah selesai';
								}
							?>
							<div class="media-body">
								<span class="title">Status :</span>
								<span class="value"><?=$status1;?></span>
							</div>
						</div>
						<!--<p class="footer text-success"><i class="fa fa-caret-up"></i> 5% <span>Compared to last week</span></p>-->
					</div>
				</div>
			</div>
		</div>
		<p class="text-left"><a class='btn btn-warning' href="<?php echo site_url('order');?>"><i class='fa fa-chevron-left'>&nbsp;Back</i></a></p>
	</div>
		
	</div>
</div>
<script>
$(document).ready(function(){
	$(".menus").removeClass("active");
	$("#order").addClass("active");
	
	$("#not_found").hide();
	 $("#map-canvas").hide();
	
	var lat1 = '<?=$detail[0]->lat;?>';
	var lo1 = '<?=$detail[0]->lon;?>';
	var geocoder = new google.maps.Geocoder();
    function initialize() {
      var map;
      var position = new google.maps.LatLng(lat1, lo1);    // set your own default location.
      var myOptions = {
        zoom: 15,
        center: position
      };

      var map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
	  
        var marker = new google.maps.Marker({
            position: position, 
            map: map,
			//title: position.name,
            title: "You are here."
        });
    }	
	if(lat1 != '' && lo1 != ''){
		 $("#map-canvas").show();
		 google.maps.event.addDomListener(window, 'load', initialize);
	}else{
		$("#not_found").show();
	}
});

function chg_stat(stat)
{
	var id = $(stat).attr("data");
	var val = $(stat).val();
	alert('ini '+id+' val '+val);
	//location.href = "<?php echo site_url('category/swap/'); ?>"+data1+"/"+stat; 
}
</script>