<h1 class="page-title">Welcome Agent</h1>
<!-- DRAG/DROP COLUMNS REORDER -->
<div class="panel">
	<div class="panel-heading">
		<h3 class="panel-title">Grafik Keuntungan</h3>
	</div>
	<div class="panel-body">
		<!--<p class="alert alert-info"><i class="fa fa-info-circle"></i> Try drag and drop the column to another position to reorder table columns.</p>-->
		<div class="col-md-10">
		<!-- VISITS VS SALES -->
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Grafik penjualan / bulan.</h3>
			</div>
			<div class="panel-body">
				<div class="demo-flot-chart" id="demo-flot-line-chart"></div>
			</div>
			</div>
		</div>
		
		
	</div>
</div>

<script src="<?php echo base_url(); ?>component/theme/assets/vendor/Flot/jquery.flot.js"></script>
<script src="<?php echo base_url(); ?>component/theme/assets/vendor/Flot/jquery.flot.resize.js"></script>
<script src="<?php echo base_url(); ?>component/theme/assets/vendor/Flot/jquery.flot.time.js"></script>
<script src="<?php echo base_url(); ?>component/theme/assets/vendor/flot.tooltip/jquery.flot.tooltip.js"></script>

<script>
$(document).ready(function(){
	$(".menus").removeClass("active");
	$("#agent").addClass("active");
});

$(function() {
		var plot;

		var jual = <?php echo json_encode($jual); ?>;
		var thn = <?php echo $tahun; ?>;
		var rjual = [];
		var a = 0;
		for(i = 1; i < 13; i++){
			var nil = "";
			nil = [gt(thn, i, 1), jual[a]];
			rjual.push(nil); 
			a++;
		};
		
		/*val = [
			[gt(2013, 1, 1), 100],
			[gt(2013, 2, 1), 155],
			[gt(2013, 3, 1), 180],
			[gt(2013, 4, 1), 172],
			[gt(2013, 5, 1), 222],
			[gt(2013, 6, 1), 300],
			[gt(2013, 7, 1), 550],
			[gt(2013, 8, 1), 452],
			[gt(2013, 9, 1), 552],
			[gt(2013, 10, 1), 600],
			[gt(2013, 11, 1), 680],
			[gt(2013, 12, 1), 750]
		];*/

		// Line Chart
		plot = $.plot($('#demo-flot-line-chart'), [{
				data: rjual,
				label: "Penjualan"
			}],

			{
				series: {
					lines: {
						show: true,
						lineWidth: 2,
						fill: false
					},
					points: {
						show: true,
						lineWidth: 3,
						fill: true,
						fillColor: "#fafafa"
					},
					shadowSize: 0
				},
				grid: {
					hoverable: true,
					clickable: true,
					borderWidth: 0,
					tickColor: "#E4E4E4",

				},
				colors: ["#5399D6"],
				xaxis: {
					mode: "time",
					timezone: "browser",
					minTickSize: [1, "month"],
					font: {
						color: "#676a6d"
					},
					tickColor: "#fafafa",
					autoscaleMargin: 0.02
				},
				yaxis: {
					font: {
						color: "#676a6d"
					},
					ticks: 8,
				},
				legend: {
					labelBoxBorderColor: "transparent",
					backgroundColor: "transparent"
				},
				tooltip: true,
				tooltipOpts: {
					content: '%s: %y'
				}
			}
		);
		
		// get day function
		function gt(y, m, d) {
			return new Date(y, m - 1, d).getTime();
		}
});
</script>