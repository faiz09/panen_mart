        <header id="header" class="header-v1 header-v3">
            <div id="topbar">
                <div class="container container-ver2">
                    <div class="inner-topbar box">
                   
                        <div class="logo">
                            <a href="#" title="Panen Mart">
                                <img src="<?php echo base_url();?>component/img/logo.png" alt="Panen Mart">
                            </a>
                        </div>
                        
                    </div>
                </div>
                <!-- End container -->
            </div>
            <!-- End topbar -->
            <div class="header-top">
                    <div class="container container-ver2">
                    <div class="box">
                        <p class="icon-menu-mobile"><i class="fa fa-bars" ></i></p>
                        <div class="logo-mobile"><a href="#" title="Panemart"><img src="<?php echo base_url();?>component/img/logo-typo2.png" alt="Xanadu-Logo"></a></div>
                        <nav class="mega-menu">
                        <!-- Brand and toggle get grouped for better mobile display -->
                          <ul class="nav navbar-nav" id="navbar">
                              <li class="level1"><a href="<?php echo site_url('main/index');?>" title="Beranda">Beranda</a></li>
                              <li class="level1"><a href="<?php echo site_url('main/about_us');?>" title="Tentang Kami">Tentang Kami</a></li>
                          </ul>
                        </nav>
                    </div>                
                    </div>
                    <!-- End container -->
            </div>
                <!-- End header-top -->
        </header><!-- /header -->
			</br>
            <div class="tp-banner-container hidden-dot ver1">
            <div class="tp-banner" >
                <ul>    <!-- SLIDE  -->
                    <!-- SLIDE  -->
                    <li data-transition="fade" data-slotamount="6" data-masterspeed="1000" >
                        <!-- MAIN IMAGE -->
                        <img src="<?php echo base_url();?>component/assets/images/home1-slideshow4.jpg"  alt="Futurelife-home2-slideshow"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                
                        
                        <!-- LAYER NR. 9 -->
                        <div class="tp-caption color-white customin randomrotateout font-ro tp-resizeme size-100 weight-300 uppercase" style="font-weight:900;"
                            data-x="515"
                            data-y="245"
                            data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                            data-speed="500"
                            data-start="2000"
                            data-easing="Power4.easeOut"
                            data-splitin="chars"
                            data-splitout="none"
                            data-elementdelay="0.1"
                            data-endelementdelay="0.1"
                            style="z-index: 3">Segar Dan Lokal
                        </div>

                        <!-- LAYER NR. 3 -->
                        <!--<div class="tp-caption color-ea8800 font-ros weight-400 skewfromleft customout size-20 letter-spacing-2"
                            data-x="850"
                            data-y="403"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="800"
                            data-start="1600"
                            data-easing="Power4.easeOut"
                            data-endspeed="300"
                            data-endeasing="Power1.easeIn"
                            data-captionhidden="on"
                            style="z-index: 4">Panenmart
                        </div>

                        <!-- LAYER NR. 7 -->
                        <!--<div class="tp-caption skewfromleft customout font-roc link-1 bg-brand color-white height-40 size-16"
                            data-x="865"
                            data-y="460"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="800"
                            data-start="1500"
                            data-easing="Power4.easeOut"
                            data-endspeed="300"
                            data-endeasing="Power1.easeIn"
                            data-captionhidden="on"
                            style="z-index: 5">
                        </div>
                        <!-- LAYER NR. 8s -->
                    </li>
                    <!-- SLIDER -->
                    <!-- SLIDE  -->
                    <li data-transition="fade" data-slotamount="6" data-masterspeed="1000" >
                        <!-- MAIN IMAGE -->
                        <img src="<?php echo base_url();?>component/assets/images/home1-slideshow2.jpg"  alt="Futurelife-home2-slideshow"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">

                        <!-- LAYER NR. 9 -->
                        <!--<div class="tp-caption color-white customin randomrotateout font-ros tp-resizeme size-60 text-shadow"
                             data-x="780"
                             data-y="305"
                             data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                             data-speed="500"
                             data-start="2000"
                             data-easing="Power4.easeOut"
                             data-splitin="chars"
                             data-splitout="none"
                             data-elementdelay="0.1"
                             data-endelementdelay="0.1"
                             style="z-index: 3">Panenmart
                        </div>

                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption color-white font-ros weight-300 skewfromleft customout size-20 letter-spacing-2 text-shadow"
                             data-x="550"
                             data-y="390"
                             data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                             data-speed="800"
                             data-start="1600"
                             data-easing="Power4.easeOut"
                             data-endspeed="300"
                             data-endeasing="Power1.easeIn"
                             data-captionhidden="on"
                             style="z-index: 4">Kami mengantarkan sayuran segar yang merupakan produk petani lokal
                        </div>

                        <!-- LAYER NR. 7 -->
                        <!--<div class="tp-caption skewfromleft customout border-white font-roc link-1 height-40 size-16"
                             data-x="865"
                             data-y="460"
                             data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                             data-speed="800"
                             data-start="1500"
                             data-easing="Power4.easeOut"
                             data-endspeed="300"
                             data-endeasing="Power1.easeIn"
                             data-captionhidden="on"
                             style="z-index: 5">
                        </div>
                        <!-- LAYER NR. 8s -->
                    </li>
                    <!-- SLIDER -->
					
					<?php
					   foreach($slider as $slide):

						  echo '
                            <li data-transition="fade" data-slotamount="6" data-masterspeed="1000" >
								<img src="'.base_url().'component/upload/slide/'.$slide->picture.'"  alt="Futurelife-home2-slideshow"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
							</li>
							';
                       endforeach; ?>
                </ul>
                <div class="tp-bannertimer"></div>
            </div>
        </div>
        <!-- End container -->
            <div class="container container-ver2 space-30">
                <div class="shipping-v2 home3-shiping space-30">
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <img src="<?php echo base_url();?>component/assets/images/icon-shipping-2.png" alt="images">
                        <h3>Mudah</h3>
                        <p>DISTRIBUSI LANGSUNG KE KONSUMEN</p>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <img src="<?php echo base_url();?>component/assets/images/icon-shipping-3.png" alt="images">
                        <h3>Menguntungkan</h3>
                        <p>HARGA YANG STABIL DAN TERJANGKAU</p>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <img src="<?php echo base_url();?>component/assets/images/icon-shipping-4.png" alt="images">
                        <h3>Terpercaya</h3>
                        <p>PANGAN SEGAR DARI PETANI LOKAL </p>
                    </div>
					<div class="col-md-3 col-sm-3 col-xs-6">
                        <img src="<?php echo base_url();?>component/assets/images/icon-shipping-1.png" alt="images">
                        <h3>Cepat</h3>
                        <p>PROSES PEMESANAN CEPAT</p>
                    </div>
                </div>
                <!-- End container -->
            </div>
            <!-- End Shiping version2  -->
			
			<div class="container container-ver2">
                <div class="choose-us choose-us-home2">
                    <div class="col-md-4">
                        <img class="img-responsive hidden-table" src="<?php echo base_url();?>component/img/kak.png" alt="banner">
                    </div>
                    <!--End col-md-4-->
                    <div class="col-md-8">
                        <div class="title-choose align-center">
                            <h3 style="font-size:280%;"><span>Apa Itu </span>PANENMART <span> ?</span></h3>
                            <p>Panenmart adalah social-enterprise yang mengembangkan solusi Teknologi dan sistem informasi khususnya petani dan masyarakat Indonesia. Melalui aplikasi berbasis mobile, Panenmart berupaya memberikan solusi perdagangan produk pangan secara online agar tercapai stabilitas harga pangan dengan memperpendek rantai distribusi dari petani hingga ke konsumen akhir dan membantu petani untuk meningkatkan akses mereka terhadap pasar agar petani sebagai produsen menjadi posisi yang lebih adil serta konsumen dapat menikmati pangan yang segar langsung dari petani.</p>
                        </div>
                        <!--End title-choose-->
                        <div class="align-center border-choose">
                            <div class="images">
                                <img src="<?php echo base_url();?>component/assets/images/bg-border-center.png" alt="icon">
                            </div>
                        </div>
					</div>
                </div>
            </div>
			
			<!--<div class="bg-slider-one-item space-50">
            <div class="slider-dot-images">-->

                <!-- End container -->
				<div class="space-50">
                <div class="container container-ver2">
                    <div class="brand-content owl-carousel">
                        <div class="items">
                            <a href="#" title="Bayam"><img class="img-responsive" src="<?php echo base_url();?>component/assets/images/sayur/brand-2.jpg" alt="Bayam"></a>

                        </div>
                        <div class="items">
                            <a href="#" title="Bawang Merah"><img class="img-responsive" src="<?php echo base_url();?>component/assets/images/sayur/bawang_merah.jpg" alt="Bawang Merah"></a>

                        </div>
                        <div class="items">
                            <a href="#" title="Kacang Panjang"><img class="img-responsive" src="<?php echo base_url();?>component/assets/images/sayur/brand-6.JPG" alt="Kacang Panjang"></a>

                        </div>
                        <div class="items">
                            <a href="#" title="Tomat"><img class="img-responsive" src="<?php echo base_url();?>component/assets/images/sayur/brand-7.jpg" alt="Tomat"></a>

                        </div>
                        <div class="items">
                            <a href="#" title="Kentang"><img class="img-responsive" src="<?php echo base_url();?>component/assets/images/sayur/brand-4.jpg" alt="Kentang"></a>

                        </div>
                        <div class="items">
                            <a href="#" title="Cabai"><img class="img-responsive" src="<?php echo base_url();?>component/assets/images/sayur/brand-5.jpg" alt="Cabai"></a>
                        </div>
						<div class="items">
                            <a href="#" title="Wortel"><img class="img-responsive" src="<?php echo base_url();?>component/assets/images/sayur/DSCF2889.JPG" alt="Wortel"></a>
                        </div>
						<div class="items">
                            <a href="#" title="Kubis"><img class="img-responsive" src="<?php echo base_url();?>component/assets/images/sayur/DSCF2911.JPG" alt="Kubis"></a>
                        </div>
						<div class="items">
                            <a href="#" title="Sayuran"><img class="img-responsive" src="<?php echo base_url();?>component/assets/images/sayur/DSCF2920.JPG" alt="Sayuran"></a>
                        </div>
                    </div>
                </div>
                <!--End container-->
            <!--</div>-->
        </div>
		
		<div class="special bg-images special-v2 box" style="background-image:url('<?php echo base_url();?>component/img/home1-banner2.jpg');background-repeat: no-repeat;background-position:right;">
            <div class="col-md-7 float-left align-right">
                
            </div>
            <!-- End col-md-7 -->
            <div class="col-md-5 float-right">
                <div class="special-content align-center">
                    <p>Download Sekarang</p>
                    <h3 style="font-size:280%;">PanenMart</h3>
                    <h5>Beli Sayuran Segar langsung dari Gadget menuju dapur anda</h5>
                    <a href="#"><img  src="<?php echo base_url();?>component/assets/images/appstore.png" alt="images" width="60%"></a>
                   
                </div>
            </div>
            <!-- End col-md-5 -->
        </div>
        <!-- End container -->
		
        <div class="choose-us">
            <div class="container container-ver2">
                <div class="title-choose align-center"></div>
                <div class="title-choose align-center"></div>
                <div class="title-choose align-center">
                    <h3>Visi Panenmart </h3>
                    <p>Mewujudkan produksi, distribusi dan konsumsi pangan secara lebih berkeadilan dengan membentuk harga pangan yang sesuai serta pasokan yang berkelanjutan.</p>
                    <div class="align-center border-choose">
                        <div class="images">
                            <img src="<?php echo base_url();?>component/assets/images/bg-border-center.png" alt="icon">
                        </div>
                        <!--End images-->
                    </div>
                    <!--End border-->
                </div>
                <!--End title-->
                <div class="col-md-3 align-left">
                    <div class="text">
                        <img class="icon-choose" src="<?php echo base_url();?>component/assets/images/icon-choose-1.png" alt="icon-choose">
                        <h3>Segar</h3>
                        <p>Pangan segar langsung dipanen oleh petani lokal. </p>
                    </div>
                    <!--End text-->
                    <div class="text">
                        <img class="icon-choose" src="<?php echo base_url();?>component/assets/images/icon-choose-2.png" alt="icon-choose">
                        <h3>Petani Makmur</h3>
                        <p>Konsumen puas petani makmur. </p>
                    </div>
                    <!--End text-->
                    <div class="text">
                        <img class="icon-choose" src="<?php echo base_url();?>component/assets/images/icon-choose-3.png" alt="icon-choose">
                        <h3>Siap Kirim</h3>
                        <p>Pangan siap kirim menuju lokasi anda. </p>
                    </div>
                    <!--End text-->
                </div>
                <!--End col-md-3-->
                <div class="col-md-6">
                    <img class="img-responsive" src="<?php echo base_url();?>component/assets/images/images-choosea.jpg" alt="banner">
                </div>
                <!--End col-md-6-->
                <div class="col-md-3 align-right right-items">
                    <div class="text">
						<img class="icon-choose" src="<?php echo base_url();?>component/assets/images/icon-choose-6.png" alt="icon-choose">
                        <h3>Dari petani</h3>
                        <p>Produk langsung dari petani. </p>
                    </div>
                    <!--End text-->
                    <div class="text">
                        <img class="icon-choose" src="<?php echo base_url();?>component/assets/images/icon-choose-5.png" alt="icon-choose">
                        <h3>Proses Cepat</h3>
                        <p>Hanya dengan sentuhan jari, pangan anda segera dikirim </p>
                    </div>
                    <!--End text-->
                    <div class="text">
						<img class="icon-choose" src="<?php echo base_url();?>component/assets/images/icon-choose-4.png" alt="icon-choose">
                        <h3>Harga Stabil</h3>
                        <p>Hanya di Panemart belanja pangan dengan harga tidak naik turun. </p>
                    </div>
                    <!--End text-->
                </div>
                <!--End col-md-3-->
            </div>
            <!--End container-->
        </div>
        <!--End choose-us-->
        
		
		<div class="bg-slider-one-item space-50">

            <div class="slider-dot-images">
                <div class="container container-ver2 center">
                    <div class="slider-nav">
                        <div>
                            <img src="<?php echo base_url();?>component/img/petani2/petani1.png" alt="Petani">
                        </div>
                        <div>
                            <img src="<?php echo base_url();?>component/img/petani2/petani2.png" alt="Petani">
                        </div>
                        <div>
                            <img src="<?php echo base_url();?>component/img/petani2/petani3.png" alt="Petani">
                        </div>
                        <div>
                            <img src="<?php echo base_url();?>component/img/petani2/petani4.png" alt="Petani">
                        </div>
                        <div>
                            <img src="<?php echo base_url();?>component/img/petani2/petani5.png" alt="Petani">
                        </div>
                        <div>
                            <img src="<?php echo base_url();?>component/img/petani2/petani6.png" alt="Petani">
                        </div>
                    </div>

                    <div class="slider-for">
                        <div>
                            <p>Saya sangat bersyukur, dengan bekerja sama panenmart memudahkan pendistribusian sayuran yang saya tanam.</p>
                            <h3>Pak Adi</h3>
                            <a href="#" title="Petani">Petani</a>
                        </div>
                        <div>
                            <p>Sayuran segar langsung dari saya. Bisa anda dapatkan di panenmart.</p>
                            <h3>Mas Dadang</h3>
                            <a href="#" title="Petani">Petani</a>
                        </div>
                        <div>
                            <p>Sayuran langsung dari petani & melestarikan petani lokal.</p>
                            <h3>Pas Suhardi</h3>
                            <a href="#" title="Petani">Petani</a>
                        </div>
                        <div>
                            <p>Alhamdulillah lebih mudah dan menguntungkan untuk memasarkan sayuran.</p>
                            <h3>Bu Nuni</h3>
                            <a href="#" title="Petani">Petani</a>
                        </div>
                        <div>
                            <p>Penjualan yang lebih menjangkau luas masyarakat dan sesuai dengan kehidupan modern.</p>
                            <h3>Pak Bowo</h3>
                            <a href="#" title="Petani">Petani</a>

                        </div>
                        <div>
                            <p>Alhamdulillah ya mas.</p>
                            <h3>Mas Andi</h3>
                            <a href="#" title="Petani">Petani</a>
                        </div>
                    </div>

                    <!-- End slider-for -->
                </div>

                <!--End container-->
            </div>

        </div>
        
            <div class="container container-ver2">
                <div class="border-title"></div>
            </div>
            <!--End container-->
            <div class="container container-ver2 blog-home1">
                    <div class="title-text-v2">
                        <div class="icon-title align-center space-20">
                            <img src="<?php echo base_url();?>component/assets/images/title-lastest-from.png" alt="icon-title">
                        </div>  
                        <h3>Blog Panenmart</h3>
                        <!--<a class="link padding-bt-20" href="#" title="See all">See all</a>-->
                    </div>
                    <!-- End title -->
                    <div class="blog-content owl-carousel slider-three-item">
						
						<?php
					   foreach($blog as $news):
						  $tgl_z = explode(" ",$news->tanggal);
						  date_default_timezone_set("Asia/Makassar");
						  $blog_baru = date('d/m/Y', strtotime(str_replace('-','/',$tgl_z[0])));
						  echo "
							<div class='item'>
								<a class='hover-images' href='".site_url('main/blog')."/$news->idEn' title='$news->judul'>
									<img class='' src='".base_url()."component/upload/blog/$news->pic' alt='blog' max-height='320' max-width='500' >
								</a>
								<div class='text'>
									<p class='date'>$blog_baru</p>
									<a href='".site_url('main/blog')."/$news->idEn' title='$news->judul'><h3>$news->judul</h3></a>
									<p class='by'>Write by <span>$news->penulis</span></p>
								</div>
							</div>";
                          endforeach; ?>
						
                        <!--<div class="item">
                            <a class="hover-images" href="#" title="images">
                                <img class="img-responsive" src="<?php echo base_url();?>component/assets/images/ImgBlog/1.jpg" alt="blog">
                            </a>
                            <div class="text">
                                <p class="date">DEC 17,2016</p>
                                <a href="<?php echo site_url('main/blog');?>" title="title"><h3>Learn On Organic Farms</h3></a>
                                <p class="by">Post by <span>FreshFood</span> - 16 Comments</p>
                            </div>
                        </div>
                        <!-- End item -->
                        <!--<div class="item">
                            <a class="hover-images" href="#" title="images">
                                <img class="img-responsive" src="<?php echo base_url();?>component/assets/images/ImgBlog/1.jpg" alt="blog">
                            </a>
                            <div class="text">
                                <p class="date">DEC 17,2016</p>
                                <a href="<?php echo site_url('main/blog');?>" title="title"><h3>What is organic farming?</h3></a>
                                <p class="by">Post by <span>FreshFood</span> - 16 Comments</p>
                            </div>
                        </div>
                        <!-- End item -->
                        <!--<div class="item">
                            <a class="hover-images" href="#" title="images">
                                <img class="img-responsive" src="<?php echo base_url();?>component/assets/images/ImgBlog/1.jpg" alt="blog">
                            </a>
                            <div class="text">
                                <p class="date">DEC 17,2016</p>
                                <a href="<?php echo site_url('main/blog');?>" title="title"><h3>Advantages of Organic Meat</h3></a>
                                <p class="by">Post by <span>FreshFood</span> - 16 Comments</p>
                            </div>
                        </div>
                        <!-- End item -->
                    </div>
                    <!-- End blog-content owl-carousel -->
            </div>
            <!-- End container -->
        