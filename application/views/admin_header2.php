<!doctype html>
<html lang="en">

<head>
	<title>Agent Page</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>component/theme/assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>component/theme/assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>component/theme/assets/vendor/linearicons/style.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>component/theme/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css">
	<script src="<?php echo base_url(); ?>component/theme/assets/vendor/jquery/jquery.min.js"></script>
	<link rel="stylesheet" href="<?php echo base_url(); ?>component/theme/assets/vendor/parsleyjs/css/parsley.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>component/theme/assets/vendor/datatables/css-main/jquery.dataTables.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>component/theme/assets/vendor/datatables/css-bootstrap/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>component/theme/assets/vendor/datatables-tabletools/css/dataTables.tableTools.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>component/theme/assets/vendor/summernote/summernote.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>component/theme/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">
	<link href="<?php echo base_url(); ?>component/sweetalert/sweetalert.css" rel="stylesheet">
	<script type="text/javascript" src="<?php echo base_url('component/sweetalert/sweetalert.min.js'); ?>"></script>
	<link rel="stylesheet" href="<?php echo base_url(); ?>component/theme/assets/vendor/toastr/toastr.min.css">
	<script src="<?php echo base_url(); ?>component/ckeditor/ckeditor.js"></script>
	<link rel="stylesheet" href="<?php echo base_url(); ?>component/theme/assets/vendor/dropify/css/dropify.min.css">
	
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>component/theme/assets/css/main.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyB3m9bT9o8gNY9PrZgHJqW34kgu9jC9juI"></script>
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>component/theme/assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>component/theme/assets/img/favicon.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="brand">
				<a href="<?php echo site_url('admin');?>"><img src="<?php echo base_url(); ?>component/img/logo-typo.png" alt="Klorofil Logo" class="img-responsive logo"></a>
			</div>
			<div class="container-fluid">
				<div id="tour-fullwidth" class="navbar-btn">
					<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
				</div>
				<div id="navbar-menu">
					<ul class="nav navbar-nav navbar-right">
						
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span><?php echo $user;?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
							<ul class="dropdown-menu">
								<li><a href="<?php echo site_url('login/logout');?>"><i class="lnr lnr-exit"></i> <span>Logout</span></a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<div id="sidebar-nav" class="sidebar" style="background-color:#2B333E;">
			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">
						<li>
							<a class="menus" id="agent" href="<?php echo site_url('agent');?>"><i class="lnr lnr-home"></i> <span>Home</span></a>
						</li>
						<li>
							<a class="menus" id="order" href="<?php echo site_url('order2');?>"><i class="lnr lnr lnr-map"></i> <span>Order</span></a>
						</li>
						<li>
							<a class="menus" id="stok" href="<?php echo site_url('stok2');?>"><i class="lnr lnr-sync"></i> <span>Stok</span></a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!--<ul class="breadcrumb">
						<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
						<li><a href="#">Tables</a></li>
						<li class="active">Dynamic Tables</li>
					</ul>-->
					