  <header id="header" class="header-v3 header-v2">
			<div class="header-top">
                <div class="container container-ver2">
                    <div class="box">
                        <p class="icon-menu-mobile"><i class="fa fa-bars" ></i></p>
                        <div class="logo"><a href="#" title="Panen Mart">
                            <img src="<?php echo base_url();?>component/img/logo-typo2.png" alt="images">
                        </a></div>
                        <div class="logo-mobile"><a href="#" title="Panenmart"><img src="<?php echo base_url();?>component/img/logo-typo2.png" alt="Panenmart"></a></div>
						<nav class="mega-menu float-left">
                        <!-- Brand and toggle get grouped for better mobile display -->
                          <ul class="nav navbar-nav" id="navbar">
							  <li class="level1">&nbsp;</li>
							  <li class="level1"><a href="<?php echo site_url('main/index');?>" title="Beranda">Beranda</a></li>
                              <li class="level1"><a href="<?php echo site_url('main/about_us');?>" title="Tentang Kami">Tentang Kami</a></li>
                          </ul>
                        </nav>
                    </div>
                </div>
                <!-- End container -->
            </div>
            <!-- End header-top -->
        </header><!-- /header -->
		</br>
        <div class="container">
            <div class="banner-header banner-lbook3 space-30">
                <img src="<?php echo base_url();?>component/img/header.png" alt="Banner-header">
                <!--<div class="text">
                        <h3>Blog Details</h3>
                        <p><a href="<?php echo site_url('main/index');?>" title="Home">Home</a><i class="fa fa-caret-right"></i>Blog Details</p>
                    </div>-->
            </div>
        </div>
        <div class="container container-ver2">
            <div class="blog-post-container blog-page blog-classic">
            <div id="primary" class="col-xs-12 col-md-10 float-left center">
                <div class="blog-post-container box single-post">
                    <div class="blog-post-item">
                        <div class="head">
                            <h3><?php echo $blog->judul;?></h3>
							<?php
							$bulan = array(
								'01' => 'Januari',
								'02' => 'Februari',
								'03' => 'Maret',
								'04' => 'April',
								'05' => 'Mei',
								'06' => 'Juni',
								'07' => 'Juli',
								'08' => 'Agustus',
								'09' => 'September',
								'10' => 'Oktober',
								'11' => 'November',
								'12' => 'Desember'
								);
								$tanggal_time = $blog->tanggal_iat;
								$tanggalq = explode(' ', $tanggal_time);
								$split = explode('-', $tanggalq[0]);
								$bln = $split[1];
								$hari = $bulan[$bln]." ".$split[2].",".$split[0];
							?>
                            <p class="post-by"><span><?php echo $hari." ".$tanggalq[1]; ?></span><span><i class="fa fa-pencil-square-o"></i> <?php echo $blog->penulis;?></span></p>
                        </div>
                        <!-- End head -->
                        <div class="blog-post-image hover-images">
                            <a href="#" title="Post"><img class="img-responsive" src="<?php echo base_url();?>component/upload/blog/<?php echo $blog->potret;?>" alt=""></a>
                        </div>  
                        <div class="text align-left">     
							<?php echo $blog->isi; ?>
							
							</br></br>
                            <div class="box">
                                <h3 class="title-v1 space-30">Berita Terbaru</h3>
                                <div class="blog-post-inner blog-related owl-nav-hidden center owl-carousel">
								
									<?php
										date_default_timezone_set("Asia/Makassar");
									   foreach($rec as $recent):
										  $blog_baru = date('d/m/Y', strtotime(str_replace('-','/',$recent->tanggal)));
										  echo "
											<div class='blog-post-item'>
												<div class='blog-post-image hover-images'>
												<a href='".site_url('main/blog')."/$recent->idEn' title='$recent->judul'>
													<img src='".base_url()."component/upload/blog/$recent->pic' alt='blog' max-height='300' max-width='400' >
												</a>
												</div> 
												<div class='text'>
													<!--<p class='date'></p>-->
													<h3><a href='".site_url('main/blog')."/$recent->idEn' title='$recent->judul'>$recent->judul</a></h3>
													<p class='post-by'><span><i class='fa fa-pencil-square-o'></i> $recent->penulis</span><span><i class='fa fa-calendar'></i> $blog_baru</span></p>
												</div>
											</div>";
										 endforeach; ?>
								</div>
										 
                            </div>
                                <!-- End Blog3ItemSlider -->
                         </div>
                        </div>
                        <!-- End text -->
                    </div>
                    <!-- End item -->                    
                </div>
        </div>
        <!-- end product sidebar -->
        </div>
        <!-- End cat-box-container -->