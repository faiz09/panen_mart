<h1 class="page-title">Edit Category</h1>
<div class="row">
<div class="col-md-12">
	<div class="panel">
		<div class="panel-heading">
			<!--<h3 class="panel-title"></h3>-->
		</div>
		<div class="panel-body">
		<div class="form-horizontal">
			</br>
			<form id="form-tambah_blog" class="col-md-10" data-parsley-validate novalidate role="form" method="POST" enctype="multipart/form-data" >
				<div class="form-group">
					<label class="col-md-3 control-label">Nama :</label>
					<div class="col-md-9">
						<input type="text" id="nama" name="nama" class="form-control" value="<?php echo set_value('nama', $nama)?>" required>
						<?php echo form_error('nama', '<p class="text-danger">');?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Urutan :</label>
					<div class="col-md-9">
						<?php
							$style_urutan='class="form-control" id="urutan"';
							echo form_dropdown("urutan",$urutan1,'',$style_urutan);
						?>
						<?php echo form_error('urutan', '<p class="text-danger">');?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Tanggal Ditambahkan :</label>
					<div class="col-md-9">
						<div class="input-group date" data-date-autoclose="true" data-provide="datepicker" data-date-format="yyyy/mm/dd">
							<input type="text" id="tanggal" name="tanggal" class="form-control" value="<?php echo set_value('tanggal', $tanggal)?>" class="form-control" required>
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						</div>
						<?php echo form_error('tanggal', '<p class="text-danger">'); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Gambar :</label>
					<? if($pic == ""){
							$h = "hidden";
							$s = "";
						}else{
							$s = "hidden";
							$h = "";
						}
					?>
					<div class="col-md-9 <?php echo $h; ?> has-pic">
						<div class="view-message contact-info">
							<img src="<?php echo base_url(); ?>component/upload/category/<?php echo $pic; ?>" class="user-image" alt="Gambar" height="64" width="64">
							<span class="sender"><?php echo $pic; ?></span>
							<br>
							<button type="button" class="btn btn-primary btn-pic-c"><i class="fa fa-mail-reply"></i><span>Ganti Gambar</span></button>
						</div>
					</div>
					<div class="col-md-9 <?php echo $s; ?> no-pic">
						<input type="file" id="file" name="file" >
						<!--<p class="help-block"><em>Example block-level help text here.</em></p>-->
						<p class="help-block text-danger"><em id="fupload_error"></em></p>
						<input type="hidden" id="file_v" name="file_v" >
						<div id="filec">
						<?php echo form_error('file_v', '<p class="text-danger file_m">'); ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Status :</label>
					<div class="col-md-9">
					<?php
						if($stat == "1"){
							$p = TRUE;
							$u = "";
						}else if($stat == "2"){
							$p = "";
							$u = TRUE;
						}else{
							$p = "";
							$u = "";
						}
					?>
						<label class="fancy-radio">
							<input type="radio" name="status" value="1" <?php echo  set_radio('status', '1', $p); ?> required data-parsley-errors-container="#error-radio">
							<span><i></i>Aktif</span>
						</label>
						<label class="fancy-radio">
							<input type="radio" name="status" value="0" <?php echo  set_radio('status', '2', $u); ?>>
							<span><i></i>Tdk Aktif</span>
						</label>
						<p id="error-radio"></p>
						<?php echo form_error('status', '<p class="text-danger">'); ?>
					</div>
				</div>
				<br/>
				<!--<button type="button" class="btn btn-primary" onclick='validateForm()'>Validate</button>-->
				<button type="submit" class="btn btn-primary">Update</button>
			</form>
		</div>
		</div>
	</div>
</div>
</div>
<script>
$(document).ready(function(){
	$(".menus").removeClass("active");
	$("#category").addClass("active");
	$('#urutan').val('<?php echo $urutan; ?>').attr("selected", "selected");
});

	$(document).ready(function(){
		$(".btn-pic-c").click(function(){
			$('.no-pic').removeClass('hidden');
			$(".has-pic").fadeOut();
			$(".no-pic").fadeIn();
		});
		var flx = document.getElementsByClassName('file_m');
		if(flx.length > 0){
			$('.no-pic').removeClass('hidden');
			$('.has-pic').addClass('hidden');
		}
		var ck = "<?php echo $ck; ?>";
		if(ck == "3"){
			toastr.error('Data Gagal Diupdate.', {timeOut: 5000});
		}
	});

	$('#file').bind('change', function() {
		var a =0;
		var error = "";
		var ext = $('#file').val().split('.').pop().toLowerCase();
		if ($.inArray(ext, ['png','jpg','jpeg']) == -1){
			error = error+"Ekstensi File Bukan .png, .jpg & .jpeg";
		}else{
			a = a+1;
		}
		var picsize = (this.files[0].size);
		if (picsize > 100000){
			if(a == 1){
				error = error+"Ukuran File Melebihi 100 Kb.";
			}else{
				error = error+" Serta Ukuran File Melebihi 100 Kb.";
			}
		}else{
			a = a+1;
		}

		if(a == 2){
			document.getElementById('fupload_error').innerHTML = "<p class='text-success'>Memenuhi Ukuran Dan Ekstensi Yang Ditetapkan</p>";
		}else{
			document.getElementById('fupload_error').innerHTML = "<p class='text-danger'>"+error+"</p>";
		}
		document.getElementById('file_v').value = error;
		document.getElementById('filec').innerHTML = "";
		});
	</script>