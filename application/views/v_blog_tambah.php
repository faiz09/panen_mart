<h1 class="page-title">Tambah Blog</h1>
<div class="row">
<div class="col-md-12">
	<div class="panel">
		<div class="panel-heading">
			<!--<h3 class="panel-title"></h3>-->
		</div>
		<div class="panel-body">
		<div class="form-horizontal">
			<form id="form-tambah_blog" class="col-md-10" data-parsley-validate novalidate role="form" method="POST" enctype="multipart/form-data">
				<div class="form-group">
					<label class="col-md-2 control-label">Judul :</label>
					<div class="col-md-10">
						<input type="text" id="judul" name="judul" class="form-control" value="<?php echo set_value('judul')?>" required>
						<?php echo form_error('judul', '<p class="text-danger">');?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">Penulis :</label>
					<div class="col-md-10">
						<input type="text" id="penulis" name="penulis" class="form-control" value="<?php echo set_value('penulis')?>" required>
						<?php echo form_error('penulis', '<p class="text-danger">'); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">Isi :</label>
					<div class="col-md-10">
						<textarea name="isi" id="isi" class="form-control" style="height:160px;"><?php echo set_value('isi')?></textarea>
						<?php echo form_error('isi', '<p class="text-danger">'); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">Potret :</label>
					<div class="col-md-10">
						<input type="file" id="file" name="file" >
						<!--<p class="help-block"><em>Example block-level help text here.</em></p>-->
						<p class="help-block text-danger"><em id="fupload_error"></em></p>
						<input type="hidden" id="file_v" name="file_v" >
						<div id="filec">
						<?php echo form_error('file', '<p class="text-danger">'); ?>
						<?php echo form_error('file_v', '<p class="text-danger">'); ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">Status :</label>
					<div class="col-md-10">
						<label class="fancy-radio">
							<input type="radio" name="status" value="1" <?php echo  set_radio('status', '1'); ?> required data-parsley-errors-container="#error-radio">
							<span><i></i>Publish</span>
						</label>
						<label class="fancy-radio">
							<input type="radio" name="status" value="2" <?php echo  set_radio('status', '2'); ?>>
							<span><i></i>Unpublish</span>
						</label>
						<p id="error-radio"></p>
						<?php echo form_error('status', '<p class="text-danger">'); ?>
					</div>
				</div>
				<br/>
				<!--<button type="button" class="btn btn-primary" onclick='validateForm()'>Validate</button>-->
				<button type="submit" class="btn btn-primary">Tambahkan</button>
			</form>
		</div>
		</div>
	</div>
</div>
</div>
<script>
$(document).ready(function(){
	$(".menus").removeClass("active");
	$("#blog").addClass("active");
});
	CKEDITOR.replace('isi');
	
	$(function(){
	});
	
	$('#file').bind('change', function() {
		var a =0;
		var error = "";
		var ext = $('#file').val().split('.').pop().toLowerCase();
		if ($.inArray(ext, ['png','jpg','jpeg']) == -1){
			error = error+"Ekstensi File Bukan .png, .jpg & .jpeg";
		}else{
			a = a+1;
		}
		var picsize = (this.files[0].size);
		if (picsize > 500000){
			if(a == 1){
				error = error+"Ukuran File Melebihi 500 Kb.";
			}else{
				error = error+" Serta Ukuran File Melebihi 500 Kb.";
			}
		}else{
			a = a+1;
		}

		if(a == 2){
			document.getElementById('fupload_error').innerHTML = "<p class='text-success'>Memenuhi Ukuran Dan Ekstensi Yang Ditetapkan</p>";
		}else{
			document.getElementById('fupload_error').innerHTML = "<p class='text-danger'>"+error+"</p>";
		}
		document.getElementById('file_v').value = error;
		document.getElementById('filec').innerHTML = "";
		});
	</script>