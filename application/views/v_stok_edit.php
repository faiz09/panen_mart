<h1 class="page-title">Edit Stok</h1>
<div class="row">
<div class="col-md-12">
	<div class="panel">
		<div class="panel-heading">
			<!--<h3 class="panel-title"></h3>-->
		</div>
		<div class="panel-body">
		<div class="form-horizontal">
			<form id="form-edit_stok" class="col-md-10" data-parsley-validate novalidate role="form" method="POST" enctype="multipart/form-data" >
				<div class="form-group">
					<label class="col-md-3 control-label">Kategory :</label>
					<div class="col-md-9">
						<?php
							$style2='class="form-control" id="categoryq" required onChange="list_produk()"';
							echo form_dropdown("category",$category_list,'',$style2);
						?>
						<?php echo form_error('category', '<p class="text-danger">');?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Produk :</label>
					<div class="col-md-9">
						<?php
							$style='class="form-control" id="produk" required onChange="get_satuan()"';
							echo form_dropdown("produk",$produk_list,'',$style);
						?>
						<?php echo form_error('produk', '<p class="text-danger">');?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Agent :</label>
					<div class="col-md-9">
						<?php
							$style1='class="form-control" id="agent" required';
							echo form_dropdown("agent",$agent_list,'',$style1);
						?>
						<?php echo form_error('agent', '<p class="text-danger">');?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Harga Jual :</label>
					<div class="col-md-5">
						<input type="text" id="jual" name="jual" class="form-control" value="<?php echo set_value('jual', $jual)?>" required data-parsley-type="number">
						<?php echo form_error('jual', '<p class="text-danger">'); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Harga Beli :</label>
					<div class="col-md-5">
						<input type="text" id="beli" name="beli" class="form-control" value="<?php echo set_value('beli', $beli)?>" required data-parsley-type="number">
						<?php echo form_error('beli', '<p class="text-danger">'); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Jumlah :</label>
					<div class="col-md-5">
						<input type="text" id="jumlah" name="jumlah" class="form-control" value="<?php echo set_value('jumlah', $jumlah)?>" required data-parsley-type="number">
						<?php echo form_error('jumlah', '<p class="text-danger">'); ?>
					</div>
					<label class="col-md-1 control-label" id="satuan"><?php echo $satuan;?></label>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Tanggal Ditambahkan :</label>
					<div class="col-md-5">
						<div class="input-group date" data-date-autoclose="true" data-provide="datepicker" data-date-format="yyyy/mm/dd">
							<input type="text" id="tanggal" name="tanggal" class="form-control" value="<?php echo set_value('tanggal', $tanggal)?>" class="form-control" required>
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						</div>
						<?php echo form_error('tanggal', '<p class="text-danger">'); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Status :</label>
					<div class="col-md-5">
					<?php
						$p = "";
						$u = "";
						$q = "";
						if($stat == "1"){
							$p = TRUE;
						}else if($stat == "2"){
							$u = TRUE;
						}else if($stat == "3"){
							$q = TRUE;
						}
					?>
						<label class="fancy-radio">
							<input type="radio" name="status" value="1" <?php echo  set_radio('status', '1', $p); ?> required data-parsley-errors-container="#error-radio">
							<span><i></i>Ada</span>
						</label>
						<label class="fancy-radio">
							<input type="radio" name="status" value="2" <?php echo  set_radio('status', '2', $u); ?>>
							<span><i></i>Tdk Ada</span>
						</label>
						<label class="fancy-radio">
							<input type="radio" name="status" value="3" <?php echo  set_radio('status', '3', $q); ?>>
							<span><i></i>Dikembalikan</span>
						</label>
						<p id="error-radio"></p>
						<?php echo form_error('status', '<p class="text-danger">'); ?>
					</div>
				</div>
				<br/>
				<button type="submit" class="btn btn-primary">Update</button>
			</form>
		</div>
		</div>
	</div>
</div>
</div>
<script>
$(document).ready(function(){
	$(".menus").removeClass("active");
	$("#stok").addClass("active");
	$('#agent').val('<?php echo set_value('agent', $agent); ?>').attr("selected", "selected");
	$('#categoryq').val('<?php echo set_value('category', $category); ?>').attr("selected", "selected");
	$('#produk').append($("<option></option>").attr("value",<?php echo $product; ?>).text('<?php echo $product_name; ?>')); 
	$('#produk').val('<?php echo set_value('product', $product); ?>').attr("selected", "selected");
});
	
$(document).ready(function(){
	var ck = "<?php echo $ck; ?>";
	if(ck == "3"){
		toastr.error('Data Gagal Diupdate.', {timeOut: 5000});
	}
});

function list_produk()
 {
	 ne = $("#categoryq").val();
	 //alert(ne);
	 $.ajax({
		 url:"<?php echo base_url();?>index.php/stok/pilih_produk/"+ne+"",
		 success: function(response){
			$("#produk").html(response);
			//alert(response);
		 },
		 dataType:"html"
	 });
	 return false;
 }
 
 function get_satuan()
 {
	 ne = $("#produk").val();
	 //alert(ne);
	 $.ajax({
		 url:"<?php echo base_url();?>index.php/stok/get_satuan/"+ne+"",
		 success: function(response){
			$("#satuan").html(response);
			//alert(response);
		 },
		 dataType:"html"
	 });
	 return false;
 }
	</script>