<h1 class="page-title">Edit Member</h1>
<style>
#map-canvas {
  width: 500px;
  height: 400px;
}
</style>
<div class="row">
<div class="col-md-12">
	<div class="panel">
		<div class="panel-heading">
			<!--<h3 class="panel-title"></h3>-->
		</div>
		<div class="panel-body">
		<div class="form-horizontal">
			<form id="form-tambah_blog" class="col-md-10" data-parsley-validate novalidate role="form" method="POST" enctype="multipart/form-data">
				<div class="form-group">
					<label class="col-md-3 control-label">Nama Member :</label>
					<div class="col-md-9">
						<input type="text" id="nama" name="nama" class="form-control" value="<?php echo set_value('nama', $nama)?>" required>
						<?php echo form_error('nama', '<p class="text-danger">');?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Nomor Telepon :</label>
					<div class="col-md-9">
						<input type="text" id="telepon" name="telepon" class="form-control" value="<?php echo set_value('telepon', $telepon)?>" required data-parsley-type="number">
						<?php echo form_error('telepon', '<p class="text-danger">'); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Email :</label>
					<div class="col-md-9">
						<input type="email" id="email" name="email" class="form-control" value="<?php echo set_value('email', $email)?>" required>
						<?php echo form_error('email', '<p class="text-danger">'); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Jenis Kelamin :</label>
					<div class="col-md-9">
					<?php
						if($kelamin == "Laki-Laki"){
							$p = TRUE;
							$u = "";
						}else if($kelamin == "Perempuan"){
							$p = "";
							$u = TRUE;
						}else{
							$p = "";
							$u = "";
						}
					?>
						<label class="fancy-radio">
							<input type="radio" name="kelamin" value="Laki-Laki" <?php echo  set_radio('kelamin', 'Laki-Laki', $p); ?> required data-parsley-errors-container="#error-radio1">
							<span><i></i>Laki-Laki</span>
						</label>
						<label class="fancy-radio">
							<input type="radio" name="kelamin" value="Perempuan" <?php echo  set_radio('kelamin', 'Perempuan', $u); ?>>
							<span><i></i>Perempuan</span>
						</label>
						<p id="error-radio1"></p>
						<?php echo form_error('kelamin', '<p class="text-danger">'); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Tanggal Ditambahkan :</label>
					<div class="col-md-9">
						<div class="input-group date" data-date-autoclose="true" data-provide="datepicker" data-date-format="yyyy/mm/dd">
							<input type="text" id="tanggal" name="tanggal" class="form-control" value="<?php echo set_value('tanggal', $tanggal)?>" class="form-control" required>
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						</div>
						<?php echo form_error('tanggal', '<p class="text-danger">'); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Latitude :</label>
					<div class="col-md-9">
						<input type="text" id="lat" name="lat" class="form-control" value="<?php echo set_value('lat', $lat)?>" required>
						<?php echo form_error('lat', '<p class="text-danger">'); ?>
					</div>
				</div>
				<div class ="form-group">
					<label class="col-md-3 control-label">Longitude :</label>
					<div class="col-md-9">
						<input type="text" id="lon" name="lon" class="form-control" value="<?php echo set_value('lon', $lon)?>" required>
						<?php echo form_error('lon', '<p class="text-danger">'); ?>
					</div>
				</div>
				<div class ="form-group">
					<div class="col-md-3">
					</div>
					<label class="col-md-9 control-label" id="not_found">Latitude dan Longitude tidak diset sebelumnya.</label>
					<div  class="col-md-9" id="map-canvas"></div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Alamat :</label>
					<div class="col-md-9">
						<textarea name="alamat" id="alamat" class="form-control" rows="5" cols="30" required><?php echo set_value('alamat', $alamat)?></textarea>
						<?php echo form_error('alamat', '<p class="text-danger">'); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Foto :</label>
					<? if($pic == ""){
							$h = "hidden";
							$s = "";
						}else{
							$s = "hidden";
							$h = "";
						}
					?>
					<div class="col-md-9 <?php echo $h; ?> has-pic">
						<div class="view-message contact-info">
							<img src="<?php echo base_url(); ?>component/upload/user/<?php echo $pic; ?>" class="user-image" alt="Sender" height="200" width="200">
							<span class="sender"><?php echo $pic; ?></span>
							<br>
							<button type="button" class="btn btn-primary btn-pic-c"><i class="fa fa-mail-reply"></i><span>Ganti Gambar</span></button>
						</div>
					</div>
					<div class="col-md-9 <?php echo $s; ?> no-pic">
						<input type="file" id="file" name="file" >
						<!--<p class="help-block"><em>Example block-level help text here.</em></p>-->
						<p class="help-block text-danger"><em id="fupload_error"></em></p>
						<input type="hidden" id="file_v" name="file_v" >
						<div id="filec">
						<?php echo form_error('file_v', '<p class="text-danger file_m">'); ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Status :</label>
					<div class="col-md-9">
					<?php
						if($stat == "1"){
							$p = TRUE;
							$u = "";
							$c = "";
						}else if($stat == "0"){
							$p = "";
							$u = TRUE;
							$c = "";
						}else{
							$p = "";
							$u = "";
							$c = TRUE;
						}
					?>
						<label class="fancy-radio">
							<input type="radio" name="status" value="1" <?php echo  set_radio('status', '1', $p); ?> required data-parsley-errors-container="#error-radio">
							<span><i></i>Aktif</span>
						</label>
						<label class="fancy-radio">
							<input type="radio" name="status" value="0" <?php echo  set_radio('status', '0', $u); ?>>
							<span><i></i>Tdk Aktif</span>
						</label>
						<label class="fancy-radio">
							<input type="radio" name="status" value="2" <?php echo  set_radio('status', '2', $c); ?>>
							<span><i></i>Blokir</span>
						</label>
						<p id="error-radio"></p>
						<?php echo form_error('status', '<p class="text-danger">'); ?>
					</div>
				</div>
				<br/>
				<!--<button type="button" class="btn btn-primary" onclick='validateForm()'>Validate</button>-->
				<button type="submit" class="btn btn-primary">Edit</button>
			</form>
		</div>
		</div>
	</div>
</div>
</div>
<script>
$(document).ready(function(){
	$(".menus").removeClass("active");
	$("#member").addClass("active");
	
	$(".btn-pic-c").click(function(){
		$('.no-pic').removeClass('hidden');
		$(".has-pic").fadeOut();
		$(".no-pic").fadeIn();
	});
	var flx = document.getElementsByClassName('file_m');
	if(flx.length > 0){
		$('.no-pic').removeClass('hidden');
		$('.has-pic').addClass('hidden');
	}
	var ck = "<?php echo $ck; ?>";
	if(ck == "3"){
		toastr.error('Data Gagal Diupdate.', {timeOut: 5000});
	}
		
	$("#not_found").hide();
	$("#map-canvas").hide();
	
	var lat1 = '<?=$lat;?>';
	var lo1 = '<?=$lon;?>';
	var geocoder = new google.maps.Geocoder();
    function initialize() {
      var map;
      var position = new google.maps.LatLng(lat1, lo1);    // set your own default location.
      var myOptions = {
        zoom: 15,
        center: position
      };

      var map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
	  
        var marker = new google.maps.Marker({
            position: position, 
            map: map,
			draggable: true,
            title: "You are here."
        });
		
		 updateInput(position.lat(), position.lng());
		 codeLatLng(position.lat(), position.lng());

        // Add a "drag end" event handler
		google.maps.event.addListener(marker, 'dragend', function() {
		  updateInput(this.position.lat(), this.position.lng());
		  codeLatLng(this.position.lat(), this.position.lng());
		});
    }
	
	function updateInput(lat, lng){
      document.getElementById("lat").value = lat;
	  document.getElementById("lon").value = lng;
    }
	
	 function codeLatLng(lat, lng){

		var latlng = new google.maps.LatLng(lat, lng);
		geocoder.geocode({'latLng': latlng}, function(results, status) {
		  if (status == google.maps.GeocoderStatus.OK) {
		  console.log(results)
			if (results[1]) {
			 //formatted address
			 //alert(results[0].formatted_address)
			   document.getElementById("alamat").value = results[0].formatted_address
			//find country name
				 for (var i=0; i<results[0].address_components.length; i++) {
				for (var b=0;b<results[0].address_components[i].types.length;b++) {

				//there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
					if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
						//this is the object you are looking for
						city= results[0].address_components[i];
						break;
					}
				}
			}
			//city data
			//alert(city.short_name + " " + city.long_name)
			//document.getElementById("add1").value = city.long_name

			} else {
			  alert("No results found");
			}
		  } else {
			alert("Geocoder failed due to: " + status);
		  }
		});
	  }

	
	if(lat1 != '' && lo1 != ''){
		  $("#map-canvas").show();
		 google.maps.event.addDomListener(window, 'load', initialize);
	}else{
		$("#not_found").show();
	}
});
	
	$('#file').bind('change', function() {
		var a =0;
		var error = "";
		var ext = $('#file').val().split('.').pop().toLowerCase();
		if ($.inArray(ext, ['png','jpg','jpeg']) == -1){
			error = error+"Ekstensi File Bukan .png, .jpg & .jpeg";
		}else{
			a = a+1;
		}
		var picsize = (this.files[0].size);
		if (picsize > 500000){
			if(a == 1){
				error = error+"Ukuran File Melebihi 500 Kb.";
			}else{
				error = error+" Serta Ukuran File Melebihi 500 Kb.";
			}
		}else{
			a = a+1;
		}

		if(a == 2){
			document.getElementById('fupload_error').innerHTML = "<p class='text-success'>Memenuhi Ukuran Dan Ekstensi Yang Ditetapkan</p>";
		}else{
			document.getElementById('fupload_error').innerHTML = "<p class='text-danger'>"+error+"</p>";
		}
		document.getElementById('file_v').value = error;
		document.getElementById('filec').innerHTML = "";
		});
	</script>