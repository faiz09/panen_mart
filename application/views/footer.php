<div id="back-to-top">
            <i class="fa fa-long-arrow-up"></i>
        </div>
        <footer id="footer" class="footer-v3 align-left">
              <div class="container container-ver2">
                <div class="footer-inner">
                    <div class="row">
						<div class="col-md-3 col-sm-6">
							<img src="<?php echo base_url();?>component/img/rsz_2footer-logo.png" alt="Panen Mart" max-height='200'>
						</div>
                        <div class="col-md-3 col-sm-6">
                            <h3 class="title-footer">Contact Us</h3>
                            <p>Head Office - PT. Solusi Agro Nusantara Puri Taman Sari, Jl. Puri VII No.4 Makassar Sulawesi Selatan</p>
							<p>Mobile - 081220322955</p>
							<p>Email - info@panenmart.com</p>
                            <!--<a class="link-footer" href="#" title="footer">Read more <i class="fa fa-long-arrow-right"></i></a>-->
                        </div>
                        
                        <div class="col-md-3 col-sm-6">
                            <h3 class="title-footer">Get It Touch</h3>
                            <div class="social space-30">
                                <a href="#" title="t"><i class="fa fa-twitter"></i></a>
                                <a href="#" title="f"><i class="fa fa-facebook"></i></a>
                                <!--<a href="#" title="y"><i class="fa fa-youtube-play"></i></a>-->
                                <a href="#" title="g"><i class="fa fa-google"></i></a>
                            </div>
                            <h3 class="title-footer">Payment Accept</h3>
                            <a href="#" title="paypal"><img src="<?php echo base_url();?>component/assets/images/paypal-footer.png" alt="images"></a>
                        </div>
						<div class="col-md-3 col-sm-6"></div>
                        
                    </div>
                    <!-- End row -->
                  </div>
                  <!-- End footer-inner -->
              </div>
              <!-- End container -->
              <div class="footer-bottom box">
                <div class="container container-ver2">
                    <div class="box bottom">
                        <p class="float-left">Copyright &copy;2017 Panen Mart - All Rights Reserved.</p>
                    </div>
                </div>
                <!-- End container -->
             </div>
        </footer>
    </div>
    <!-- End wrappage -->
    <script type="text/javascript" src="<?php echo base_url();?>component/assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>component/assets/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>component/assets/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>component/assets/js/jquery.themepunch.plugins.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>component/assets/js/jquery.mousewheel.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>component/assets/js/jquery.zoom.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>component/assets/js/engo-plugins.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>component/assets/js/slick.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>component/assets/js/store.js"></script>
	
	
    </body>
</html>