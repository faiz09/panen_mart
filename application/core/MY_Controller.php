<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('access');
	
		$res=true;
		if(!$res)
		{
			redirect(Login_page);
		}
		$modul = $this->router->fetch_class();
		
		$res = $this->access->is_login();
		if(!$res)
		{
			redirect(Login_page);
		}
		
		$is = $this->access->is_who($modul);
		if($is == 'admin'){
			$k_modul = array("admin", "blog", "category", "order", "product", "slide", "stok", "member", "request");
			if(!in_array($modul, $k_modul)){
				redirect(Login_page);
			}
		}else if($is == 'agent'){
			$k_modul = array("agent", "order2", "stok2");
			if(!in_array($modul, $k_modul)){
				redirect(Login_page);
			}
		}else{
			redirect(Login_page);
		}
		
	}
}
?>
