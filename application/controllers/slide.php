<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Slide extends MY_Controller {
	
	public function __construct()
	   {
        // load library
		parent::__construct();
		$this->load->library('access');
		$this->load->library('form_validation');
		$this->load->model('m_slide');
    }
	
    public function index(){		
		$user = $this->access->get_user();
		$slider = $this->m_slide->get_pic();
		$this->load->view('admin_header.php', array("user"=>$user));
		$this->load->view('v_slide.php', array("pic"=>$slider));
		$this->load->view('admin_footer.php');
		
    }
	
	public function tambah_picture(){		
		$user = $this->access->get_user();
		if(empty($_FILES['file']['name']))
		{
			$this->form_validation->set_rules('file', 'Gambar', 'callback_cek_ada');
		}
		    $this->form_validation->set_rules('file_v','File_v','callback_cek_file');

		$this->load->view('admin_header.php', array("user"=>$user));
		if($this->form_validation->run() == FALSE){
			$this->load->view('v_slide_tambah.php');
	
		}else{
		
				$config['upload_path']          = dirname($_SERVER["SCRIPT_FILENAME"])."/component/upload/slide";
				$config['allowed_types']        = 'jpeg|jpg|png';
				$config['max_size']             = 500;
				
				$this->load->library('upload', $config);
		 
				if(!$this->upload->do_upload('file')){
					$file = "";
				}else{
					$data_foto = $this->upload->data();
				    $file = $data_foto['file_name'];
				}
				
				 $id = acak_id("slide_picture","id_pic");
				 $tgl = date("Y-m-d");
				 $result=0;
				
					$result = $this->m_slide->add_slide(array(
					'id_pic'  => $id['id'],
					'picture' => $file,
					'tanggal' => $tgl,
					'username' => $user));
				 
				 if($result==1){
				 redirect('slide');
				 }
				 else{
				 redirect('slide/tambah_picture');
				 }
		}
		$this->load->view('admin_footer.php');
    }
	
	function delete($id=null){
	 $pic = $this->m_slide->pic($id);
	 //var_dump($pic);
     $result=$this->m_slide->delete($id);
	 $file = dirname($_SERVER["SCRIPT_FILENAME"])."/component/upload/slide/".$pic->picture;
	 //echo $file;
     if($result){
	 unlink($file);
	 echo json_encode(array("pesan"=>"Data berhasil dihapus!!",
                                        "status"=>"sukses"));
	}else{
     echo json_encode(array("pesan"=>"Data gagal dihapus!!",
                                        "status"=>"error"));
	}
   }
	
	public function cek_ada($str)
		{
			//if($str == '' || $str == '0' || $str == NULL)
			//{
				$this->form_validation->set_message('cek_ada','<em>Ditemukan Error, Harap Mengecek Kembali Ekstensi Dan Ukuran Gambar.</em>');
				return FALSE;
			//}
			//else
			//{
			//	return TRUE;
			//}
		}
		
	public function cek_file($str)
		{
			if($str != '')
			{
				$this->form_validation->set_message('cek_file','<em>'.$str.'</em>');
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}

}
