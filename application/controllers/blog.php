<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends MY_Controller {
	
	public function __construct()
	   {
        // load library
		parent::__construct();
		$this->load->library('access');
		$this->load->library('form_validation');
		$this->load->model('m_blog');
    }
	
    public function index() {
		
		$user = $this->access->get_user();
		$blog = $this->m_blog->get_blogs();
		$this->load->view('admin_header.php', array("user"=>$user));
		$this->load->view('v_blog.php', array("news"=>$blog));
		$this->load->view('admin_footer.php');
		
    }
	
	public function test() {
		
		date_default_timezone_set("Asia/Makassar");
		$tgl = date("Y-m-d H:i:s");
		$tgl_z  = explode(" ",$tgl);
		echo $tgl_z[0];
		echo "<br><br>";
		$tmpTGLLAHIR = date('Y/m/d', strtotime(str_replace('-','/',$tgl_z[0])));
		echo $tmpTGLLAHIR;
		echo "<br><br>";
		$tm = date('H:i:s');
		echo $tm;
		echo "<br><br>";
		$tmpTGLLAHIRZ = date('Y-m-d H:i:s', strtotime(str_replace('-','/',$tmpTGLLAHIR." ".$tm)));
		echo $tmpTGLLAHIRZ;
    }
	
	public function tambah_berita() {
		$user = $this->access->get_user();
		
		$this->form_validation->set_rules('judul','Judul Blog','callback_cek_ada');
		$this->form_validation->set_rules('penulis','Nama Penulis','callback_cek_ada');
		$this->form_validation->set_rules('isi','Isi Blog','callback_cek_ada');
		$this->form_validation->set_rules('status','Status','callback_cek_ada');
		
		if(empty($_FILES['file']['name']))
		{
			$this->form_validation->set_rules('file', 'File', 'callback_cek_ada');
		}
		$this->form_validation->set_rules('file_v','File_v','callback_cek_file');
		
		
		$this->load->view('admin_header.php', array("user"=>$user));
		if ($this->form_validation->run() == FALSE){
			 $this->load->view('v_blog_tambah.php');
		}else{
				
			    //$new_name = time().$_FILES["file"]['name'];
			 //redirect('blog');
				$config['upload_path']          = dirname($_SERVER["SCRIPT_FILENAME"])."/component/upload/blog";
				$config['allowed_types']        = 'jpeg|jpg|png';
				$config['max_size']             = 500;
				//$config['file_name'] 			= $new_name;
				//$config['max_width']            = 1024;
				//$config['max_height']           = 768;
		 
				$this->load->library('upload', $config);
		 
				if (!$this->upload->do_upload('file')){
					//$error = array('error' => $this->upload->display_errors());
					//$this->load->view('v_error', $error);
					$file = "";
				}else{
					$data_foto = $this->upload->data();
					$file = $data_foto['file_name'];
				}
				
				 $judul = htmlentities($this->input->post('judul'), ENT_QUOTES);
				 $penulis = htmlentities($this->input->post('penulis'), ENT_QUOTES);
				 $id = acak_id("berita","id_berita");
				 $isi = $this->input->post('isi');
				 $stat = $this->input->post('status');
				 date_default_timezone_set("Asia/Makassar");
				 $tgl = date("Y-m-d H:i:s");
				 $url_title = url_title($judul);
				 $result=0;
				
					$result = $this->m_blog->add_blog(array(
					'id_berita' => $id['id'],
					'judul' => $judul,
					'tanggal_iat' => $tgl,
					'penulis' => $penulis,
					'isi' => $isi,
					'potret' => $file,
					'status' => $stat,
					'slug' => $url_title,
					'username' => $user));
					
				 if($result==1)
				 //echo json_encode(array("status"=>"sukses"));
				 redirect('blog');
				 else
				 redirect('blog/tambah_berita');
				//echo json_encode(array("status"=>"error"));
			 
		}
		
		$this->load->view('admin_footer.php');
		
    }
	
	function delete($id=null){
	 $pic = $this->m_blog->pic($id);
	 //var_dump($pic);
     $result=$this->m_blog->delete($id);
	 $file = dirname($_SERVER["SCRIPT_FILENAME"])."/component/upload/blog/".$pic->potret;
	 //echo $file;
     if($result){
	 unlink($file);
	 echo json_encode(array("pesan"=>"Data berhasil dihapus!!",
                                        "status"=>"sukses"));
	}else{
     echo json_encode(array("pesan"=>"Data gagal dihapus!!",
                                        "status"=>"error"));
	}
   }
   
    function edit($id=null){
	 $user = $this->access->get_user();
     $dt = $this->m_blog->edit_blog($id);
	 //var_dump($dt);
	// echo $dt->judul;
	 $stat = $this->uri->segment(4);
	 if($stat){
			$stat1 = $stat;
		}else{
			$stat1 =0;
		}
	  $tgl_z = explode(" ",$dt->tanggal_iat);
	  $tgl_ku = date('Y/m/d', strtotime(str_replace('-','/',$tgl_z[0])));
	
	 $this->form_validation->set_rules('judul','Judul Blog','callback_cek_ada');
	 $this->form_validation->set_rules('penulis','Nama Penulis','callback_cek_ada');
	 $this->form_validation->set_rules('isi','Isi Blog','callback_cek_ada');
	 $this->form_validation->set_rules('status','Status','callback_cek_ada');
	 $this->form_validation->set_rules('tanggal','Tanggal','callback_cek_ada');
	
	 $this->form_validation->set_rules('file_v','File_v','callback_cek_file');
	 
	 
        $this->load->view('admin_header', array("user"=>$user,"stat"=>$stat1));
		if ($this->form_validation->run() == FALSE){
		$this->load->view('v_blog_edit.php', array("judul"=>$dt->judul,"tanggal"=>$tgl_ku,
													"penulis"=>$dt->penulis,"isi"=>$dt->isi,
													"pic"=>$dt->potret,"stat"=>$dt->status,"ck"=>$stat1,
													"id"=>$id));
													
		}else{
				
				if(!empty($_FILES['file']['name']))
				{
					$config['upload_path']          = dirname($_SERVER["SCRIPT_FILENAME"])."/component/upload/blog";
					$config['allowed_types']        = 'jpeg|jpg|png';
					$config['max_size']             = 500;
					//$config['file_name'] 			= $new_name;
					//$config['max_width']            = 1024;
					//$config['max_height']           = 768;
			 
					$this->load->library('upload', $config);
			 
					if (!$this->upload->do_upload('file')){
						//$error = array('error' => $this->upload->display_errors());
						//$this->load->view('v_error', $error);
						$file = "";
					}else{
						$data_foto = $this->upload->data();
						$file = $data_foto['file_name'];
					}
				}else{
					$file = $dt->potret;
				}
				
				 $judul = htmlentities($this->input->post('judul'), ENT_QUOTES);
				 $penulis = htmlentities($this->input->post('penulis'), ENT_QUOTES);
				 $isi = $this->input->post('isi');
				 $stat = $this->input->post('status');
				 date_default_timezone_set("Asia/Makassar");
				 $tm = date('H:i:s');
				 $tgl =  $this->input->post('tanggal');
				 $tgl_f = date('Y-m-d H:i:s', strtotime(str_replace('-','/',$tgl." ".$tm)));
				 $url_title = url_title($judul);
				 $result=0;
				
					$data1 = array(
					'judul' => $judul,
					'tanggal_iat' => $tgl_f,
					'penulis' => $penulis,
					'isi' => $isi,
					'potret' => $file,
					'status' => $stat,
					'slug' => $url_title,
					'username' => $user);
					
					$result = $this->m_blog->update_blog($data1, $id);
					
				 if($result==1)
				 //echo json_encode(array("status"=>"sukses"));
				 redirect('blog');
				 else
				 redirect('blog/edit/$id/3');
				//echo json_encode(array("status"=>"error"));
			 
		}
		
		$this->load->view('admin_footer.php');
   }
	
	public function cek_user($user)
	{
		$pass=$this->input->post('signin-password');
		if ($user == '' && $pass == '')
		{
			$this->form_validation->set_message('cek_user','<span class="element-right label label-warning"><i class="fa fa-times-circle-o"></i>&nbsp;&nbsp;Username & Password Tidak Boleh Kosong </span>');
			return FALSE;
			
		}else if($user == '' && $pass != ''){
		
			$this->form_validation->set_message('cek_user','<span class="element-right label label-warning"><i class="fa fa-times-circle-o"></i>&nbsp;&nbsp;Username Tidak Boleh Kosong </span>');
			return FALSE;
			
		}else if($user != '' && $pass == ''){
		
			$this->form_validation->set_message('cek_user','<span class="element-right label label-warning"><i class="fa fa-times-circle-o"></i>&nbsp;&nbsp;Password Tidak Boleh Kosong </span>');
			return FALSE;
		
		}else if($user != '' && $pass != ''){
			
			$ck = $this->cekuser($user);
			if($ck == True){
				 $login = $this->access->login($user, $pass);
				 if($login==true){
						return TRUE;
					}else{
						$this->form_validation->set_message('cek_user','<span class="element-right label label-warning"><i class="fa fa-times-circle-o"></i>&nbsp;&nbsp;Password Tidak Valid </span>');
						return FALSE;
						//redirect('Login');
					}
			}else{
				$this->form_validation->set_message('cek_user','<span class="element-right label label-warning"><i class="fa fa-times-circle-o"></i>&nbsp;&nbsp;Username & Password Tidak Valid </span>');
				return FALSE;
			}
		}
	}
	
		public function cek_ada($str)
		{
			if($str == '' || $str == '0' || $str == NULL)
			{
				$this->form_validation->set_message('cek_ada','<em>%s tidak boleh kosong</em>');
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
		
		public function cek_file($str)
		{
			if($str != '')
			{
				$this->form_validation->set_message('cek_file','<em>'.$str.'</em>');
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}

}
