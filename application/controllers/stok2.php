<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Stok2 extends MY_Controller {
	
	public function __construct()
	   {
        // load library
		parent::__construct();
		$this->load->library('access');
		//$this->load->library('form_validation');
		$this->load->model('m_stok');
    }
	
    public function index(){
		$agent = $this->access->get_agent_id();
		$user = $this->access->get_user();
		$stok = $this->m_stok->get_stok2($agent);
		$this->load->view('admin_header2.php', array("user"=>$user));
		$this->load->view('v_stok2.php',array("stok"=>$stok));
		$this->load->view('admin_footer.php');
		
    }
	
	public function test_id(){		
		$id = acak_id("berita","id_berita");
		var_dump($id);
    }
	
}
