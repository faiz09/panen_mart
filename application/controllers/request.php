<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends MY_Controller {
	
	public function __construct()
	   {
        // load library
		parent::__construct();
		$this->load->library('access');
		$this->load->model('m_request');
    }
	
    public function index(){
		$user = $this->access->get_user();
		$request = $this->m_request->get_request();
		$this->load->view('admin_header.php', array("user"=>$user));
		$this->load->view('v_request.php', array("requestq"=>$request));
		$this->load->view('admin_footer.php');
    }
	
	public function approve(){
		$admin = $this->access->get_admin_id();
		$ids = $this->uri->segment(3);
		$result = $this->m_request->approve($ids,$admin);
		if($result){
			$this->m_request->to_admin($ids);
		}
		redirect('request');
		//echo $admin;
		//echo '</br>';
		//echo $ids;
    }
	
	/*
	function delete($id=null){
	 $pic = $this->m_slide->pic($id);
	 //var_dump($pic);
     $result=$this->m_slide->delete($id);
	 $file = dirname($_SERVER["SCRIPT_FILENAME"])."/component/upload/slide/".$pic->picture;
	 //echo $file;
     if($result){
	 unlink($file);
	 echo json_encode(array("pesan"=>"Data berhasil dihapus!!",
                                        "status"=>"sukses"));
	}else{
     echo json_encode(array("pesan"=>"Data gagal dihapus!!",
                                        "status"=>"error"));
	}
   }
	
	public function cek_ada($str)
		{
			//if($str == '' || $str == '0' || $str == NULL)
			//{
				$this->form_validation->set_message('cek_ada','<em>Ditemukan Error, Harap Mengecek Kembali Ekstensi Dan Ukuran Gambar.</em>');
				return FALSE;
			//}
			//else
			//{
			//	return TRUE;
			//}
		}
		
	public function cek_file($str)
		{
			if($str != '')
			{
				$this->form_validation->set_message('cek_file','<em>'.$str.'</em>');
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}*/

}
