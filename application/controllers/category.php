<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MY_Controller {
	
	public function __construct()
	   {
        // load library
		parent::__construct();
		$this->load->library('access');
		$this->load->library('form_validation');
		$this->load->model('m_category');
    }
	
    public function index(){		
		$user = $this->access->get_user();
		$category = $this->m_category->get_category();
		$this->load->view('admin_header.php', array("user"=>$user));
		$this->load->view('v_category.php', array("category"=>$category));
		$this->load->view('admin_footer.php');
		
    }
	
	public function test() {
		//$max = $this->m_category->add_max();
		$max = site_url('category/swap/');
		echo $max;
    }
	
	public function swap() {
		//$max = $this->m_category->add_max();
		$stat = $this->uri->segment(3);
		$stat2 = $this->uri->segment(4);
		//echo "empat = ".$stat.", lima = ".$stat2."</br>";
		$urutan1 = $this->m_category->ambil_urutan();
		//var_dump($urutan1);
		//echo $urutan1[$stat]."</br>";
		if($stat2 == "turun"){
			$c_with = $stat+1;
		}else if($stat2 == "naik"){
			$c_with = $stat-1;
		}
		//echo $urutan1[$c_with];
		$this->m_category->swap_all($urutan1[$stat], $urutan1[$c_with]);
		redirect('category');
    }
	
	public function tambah_category(){		
		$user = $this->access->get_user();

		$this->form_validation->set_rules('nama','Nama Category','callback_cek_ada');
		$this->form_validation->set_rules('status','Status','callback_cek_ada');
		
		if(empty($_FILES['file']['name']))
		{
			$this->form_validation->set_rules('file', 'File', 'callback_cek_ada');
		}
		$this->form_validation->set_rules('file_v','File_v','callback_cek_file');
		
		$this->load->view('admin_header.php', array("user"=>$user));
		if($this->form_validation->run() == FALSE){
			$this->load->view('v_category_tambah.php');
	
		}else{
		
				$config['upload_path']          = dirname($_SERVER["SCRIPT_FILENAME"])."/component/upload/category";
				$config['allowed_types']        = 'jpeg|jpg|png';
				$config['max_size']             = 100;
				
				$this->load->library('upload', $config);
		 
				if(!$this->upload->do_upload('file')){
					$file = "";
				}else{
					$data_foto = $this->upload->data();
				    $file = $data_foto['file_name'];
				}
				
				 $nama = htmlentities($this->input->post('nama'), ENT_QUOTES);
				 $stat = $this->input->post('status');
				 $tgl = date("Y-m-d H:i:s");
				 $id = acak_id("categori","id_category");
				 $urutan = $this->m_category->add_max();
				 $url_title = url_title($nama);
				 $result=0;
				
					$result = $this->m_category->add_category(array(
					'id_category' => $id['id'],
					'nama' => $nama,
					'gambar' => $file,
					'status' => $stat,
					'slug' => $url_title,
					'urutan' => $urutan,
					'tanggal_iat' => $tgl));
				 
				 if($result==1){
				 redirect('category');
				 }
				 else{
				 redirect('category/tambah_category');
				 }
		}
		$this->load->view('admin_footer.php');
    }
	
	public function cek_ada($str)
		{
			if($str == '' || $str == NULL)
			{
				$this->form_validation->set_message('cek_ada','<em>%s tidak boleh kosong</em>');
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
		
	public function cek_file($str)
	{
		if($str != '')
		{
			$this->form_validation->set_message('cek_file','<em>'.$str.'</em>');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	function delete($id=null){
	 $pic = $this->m_category->pic($id);
	 //var_dump($pic);
     $result=$this->m_category->delete($id);
	 $file = dirname($_SERVER["SCRIPT_FILENAME"])."/component/upload/category/".$pic->gambar;
	 //echo $file;
     if($result){
	 unlink($file);
	 echo json_encode(array("pesan"=>"Data berhasil dihapus!!",
                                        "status"=>"sukses"));
	}else{
     echo json_encode(array("pesan"=>"Data gagal dihapus!!",
                                        "status"=>"error"));
	}
   }
   
   function edit($id=null){
	 $user = $this->access->get_user();
     $dt = $this->m_category->edit_category($id);
	 $urutan1 = $this->m_category->ambil_urutan();
	 //echo "</br></br>";
	 //var_dump($urutan1);
	 //var_dump($dt);
	// echo $dt->judul;
	 $stat = $this->uri->segment(4);
	 if($stat){
			$stat1 = $stat;
		}else{
			$stat1 =0;
		}
	  $tgl_z = explode(" ",$dt->tanggal_iat);
	  $tgl_ku = date('Y/m/d', strtotime(str_replace('-','/',$tgl_z[0])));
	
	 $this->form_validation->set_rules('nama','Nama Category','callback_cek_ada');
	 $this->form_validation->set_rules('status','Status','callback_cek_ada');
	 $this->form_validation->set_rules('tanggal','Tanggal','callback_cek_ada');
	
	 $this->form_validation->set_rules('file_v','File_v','callback_cek_file');
	 
	 
        $this->load->view('admin_header', array("user"=>$user,"stat"=>$stat1));
		if ($this->form_validation->run() == FALSE){
		$this->load->view('v_category_edit.php', array("nama"=>$dt->nama,"tanggal"=>$tgl_ku,
													"pic"=>$dt->gambar,"stat"=>$dt->status,"ck"=>$stat1,
													"urutan"=>$dt->urutan,"urutan1"=>$urutan1,
													"id"=>$id));
													
		}else{
				
				if(!empty($_FILES['file']['name']))
				{
					$config['upload_path']          = dirname($_SERVER["SCRIPT_FILENAME"])."/component/upload/category";
					$config['allowed_types']        = 'jpeg|jpg|png';
					$config['max_size']             = 100;
			 
					$this->load->library('upload', $config);
			 
					if (!$this->upload->do_upload('file')){
						$file = "";
					}else{
						$data_foto = $this->upload->data();
						$file = $data_foto['file_name'];
					}
				}else{
					$file = $dt->gambar;
				}
				
				 $nama = htmlentities($this->input->post('nama'), ENT_QUOTES);
				 $stat = $this->input->post('status');
				 $urutan_b = $this->input->post('urutan');
				 date_default_timezone_set("Asia/Makassar");
				 $tm = date('H:i:s');
				 $tgl =  $this->input->post('tanggal');
				 $tgl_f = date('Y-m-d H:i:s', strtotime(str_replace('-','/',$tgl." ".$tm)));
				 $url_title = url_title($nama);
				 if($dt->urutan == $urutan_b){
					 $urutan = $urutan_b;
				 }else{
					 $urutan = $urutan_b;
					 $swap = $this->m_category->swap($urutan_b, $dt->urutan);
				 }
			
				 $result=0;
				
					$data1 = array(
					'nama' => $nama,
					'tanggal_iat' => $tgl_f,
					'gambar' => $file,
					'status' => $stat,
					'urutan' => $urutan,
					'slug' => $url_title);
					
					$result = $this->m_category->update_category($data1, $id);
					
				 if($result==1)
				 //echo json_encode(array("status"=>"sukses"));
				 redirect('category');
				 else
				 redirect('category/edit/$id/3');
				//echo json_encode(array("status"=>"error"));
			 
		}
		
		$this->load->view('admin_footer.php');
   }

}
