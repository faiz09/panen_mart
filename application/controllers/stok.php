<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Stok extends MY_Controller {
	
	public function __construct()
	{
        // load library
		parent::__construct();
		$this->load->library('access');
		$this->load->library('form_validation');
		$this->load->model('m_stok');
    }
	
    public function index(){		
		$user = $this->access->get_user();
		$stok = $this->m_stok->get_stok();
		$this->load->view('admin_header.php', array("user"=>$user));
		$this->load->view('v_stok.php', array("stok"=>$stok));
		$this->load->view('admin_footer.php');
		
    }
	
	 public function test_id() {		
		$id = acak_id("adm_user","id_admin");
		var_dump($id);
		//echo md5('rivo123');
    }
	
	public function pilih_produk(){
		$data['produk']=$this->m_stok->ambil_produk($this->uri->segment(3));
		$this->load->view('list_produk.php',$data);
	}
	
	public function get_satuan(){
		$satuan=$this->m_stok->ambil_satuan($this->uri->segment(3));
		echo $satuan;
		//var_dump($satuan);
	}
	
	public function tambah_stok(){		
		$user = $this->access->get_user();
		
		$produk = array('' => 'Pilih Produk');
		$agent = $this->m_stok->ambil_agent();
		$category = $this->m_stok->ambil_category();
		
		$this->form_validation->set_rules('category','Kategori','callback_cek_ada');
		$this->form_validation->set_rules('produk','Produk','callback_cek_ada');
		$this->form_validation->set_rules('agent','Agent','callback_cek_ada');
		$this->form_validation->set_rules('jual','Harga Jual','callback_cek_ada');
		$this->form_validation->set_rules('beli','Harga Beli','callback_cek_ada');
		$this->form_validation->set_rules('jumlah','Jumlah','callback_cek_ada');

		$this->load->view('admin_header.php', array("user"=>$user));
		if($this->form_validation->run() == FALSE){
			$this->load->view('v_stok_tambah.php', array("category"=>$category, "agent"=>$agent, "produk"=>$produk));
	
		}else{
				
				 date_default_timezone_set("Asia/Makassar");
				 $tgl = date("Y-m-d H:i:s");
				 $kategori = htmlentities($this->input->post('category'), ENT_QUOTES);
				 $produk = htmlentities($this->input->post('produk'), ENT_QUOTES);
				 $agent = htmlentities($this->input->post('agent'), ENT_QUOTES);
				 $id = acak_id("stok","id_stok");
				 $jual = $this->input->post('jual');
				 $beli = $this->input->post('beli');
				 $jumlah = $this->input->post('jumlah');
				 $stat = '1';
				 $result=0;
				
					$result = $this->m_stok->add_stok(array(
					'id_stok' => $id['id'],
					'id_category' => $kategori,
					'id_produk' => $produk,
					'id_agent' => $agent,
					'harga_jual' => $jual,
					'harga_beli' => $beli,
					'jumlah' => $jumlah,
					'status' => $stat,
					'tanggal_iat' => $tgl));
				 
				 if($result==1){
				 redirect('stok');
				 }
				 else{
				 redirect('stok/tambah_stok');
				 }
		}
		$this->load->view('admin_footer.php');
    }
	
	public function cek_ada($str)
		{
			if($str == '' || $str == '0' || $str == NULL)
			{
				$this->form_validation->set_message('cek_ada','<em>%s tidak boleh kosong</em>');
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
	
	function delete($id=null){
     $result=$this->m_stok->delete($id);
     if($result){
	 echo json_encode(array("pesan"=>"Data berhasil dihapus!!",
                                        "status"=>"sukses"));
	}else{
     echo json_encode(array("pesan"=>"Data gagal dihapus!!",
                                        "status"=>"error"));
	}
   }
	
	
	function edit($id=null){
	 $user = $this->access->get_user();
     $dt = $this->m_stok->edit_stok($id);
	 $produk_list = array('' => 'Pilih Produk');
	 $agent_list = $this->m_stok->ambil_agent();
	 $category_list = $this->m_stok->ambil_category();
	 $satuan = $this->m_stok->ambil_satuan($dt->id_produk);
	 $produk_name = $this->m_stok->nama_produk($dt->id_produk);
	 
	// var_dump($dt);
	 
	 $stat = $this->uri->segment(4);
	 if($stat){
			$stat1 = $stat;
		}else{
			$stat1 =0;
		}
	  $tgl_z = explode(" ",$dt->tanggal_iat);
	  $tgl_ku = date('Y/m/d', strtotime(str_replace('-','/',$tgl_z[0])));
	
		$this->form_validation->set_rules('category','Kategori','callback_cek_ada');
		$this->form_validation->set_rules('produk','Produk','callback_cek_ada');
		$this->form_validation->set_rules('agent','Agent','callback_cek_ada');
		$this->form_validation->set_rules('jual','Harga Jual','callback_cek_ada');
		$this->form_validation->set_rules('beli','Harga Beli','callback_cek_ada');
		$this->form_validation->set_rules('jumlah','Jumlah','callback_cek_ada');
		

        $this->load->view('admin_header', array("user"=>$user,"stat"=>$stat1));
		if ($this->form_validation->run() == FALSE){
		$this->load->view('v_stok_edit.php', array("product"=>$dt->id_produk,"category"=>$dt->id_category,
													"product_name"=>$produk_name,"stat"=>$dt->status,"ck"=>$stat1,
													"satuan"=>$satuan,"agent"=>$dt->id_agent,
													"jual"=>$dt->harga_jual,"beli"=>$dt->harga_beli,
													"jumlah"=>$dt->jumlah,"tanggal"=>$tgl_ku,
													"produk_list"=>$produk_list,"agent_list"=>$agent_list,
													"category_list"=>$category_list,
													"id"=>$id));
													
		}else{
				 
				 $kategori = htmlentities($this->input->post('category'), ENT_QUOTES);
				 $produk = htmlentities($this->input->post('produk'), ENT_QUOTES);
				 $agent = htmlentities($this->input->post('agent'), ENT_QUOTES);
				 $jual = $this->input->post('jual');
				 $beli = $this->input->post('beli');
				 $stat = $this->input->post('status');
				 $jumlah = $this->input->post('jumlah');
				 date_default_timezone_set("Asia/Makassar");
				 $tm = date('H:i:s');
				 $tgl =  $this->input->post('tanggal');
				 $tgl_f = date('Y-m-d H:i:s', strtotime(str_replace('-','/',$tgl." ".$tm)));
			
				 $result=0;
				
					$data1 = array(
					'id_category' => $kategori,
					'id_produk' => $produk,
					'id_agent' => $agent,
					'harga_jual' =>  $jual,
					'harga_beli' => $beli,
					'jumlah' => $jumlah,
					'tanggal_iat' => $tgl_f,
					'status' => $stat);
					
					$result = $this->m_stok->update_stok($data1, $id);
					
				 if($result==1)
				 //echo json_encode(array("status"=>"sukses"));
				 redirect('stok');
				 else
				 redirect('stok/edit/$id/3');
				//echo json_encode(array("status"=>"error"));
			 
		}
		
		$this->load->view('admin_footer.php');
   }
   
}
