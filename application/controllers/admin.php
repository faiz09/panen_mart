<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {
	
	public function __construct()
	{
        // load library
		parent::__construct();
		$this->load->library('access');
		$this->load->model('m_admin');
    }
	
    public function index(){		
		$user = $this->access->get_user();
		//$admin = $this->access->get_admin_id();
		//$adm = $this->myencryption->encode($admin);
		//$dt = $this->m_admin->edit_admin($adm);
		//var_dump($dt);
		//echo $dt->userNama;
		$array = array();
		$array1 = array();
		date_default_timezone_set("Asia/Makassar");
		$tm = date('Y');
		$i = 1;
		
		while($i < 13){
			$rata2 = $this->m_admin->ambil_penjualan($i,$tm);
			$rata = round($rata2->beli);
			$rata1 = round($rata2->jual);
			array_push($array,$rata);
			array_push($array1,$rata1);
			$i++;
		}
		//var_dump($array);
		//echo "</br></br>";
		//var_dump($array1);
		//$rata2 = $this->m_admin->ambil_penjualan('06','2017');

		$this->load->view('admin_header.php', array("user"=>$user));
		$this->load->view('v_admin.php', array("beli"=>$array,"jual"=>$array1,"tahun"=>$tm));
		$this->load->view('admin_footer.php');
		
    }
	
	public function edit_admin()
	{
		echo "Admin Terupdate";
	}
	
	public function test()
	{
		$data = "buka";
		$data1 = do_hash($data);
		//$id = acak_id("adm_user","id_admin");
		//var_dump($id);
		//$aa = $this->access->testq('admin');
		//$sc = compare_hash($data, $data2, false);
		var_dump($data1);
	}

}
