<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
	 
	 function __construct()
	   {
        // load library
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('access');
		$this->load->model('m_main');
		}
		
		
	public function index()
	{
		$blog = $this->m_main->get_blogs();
		$slide = $this->m_main->get_slide();
		$this->load->view('header.php');
		$this->load->view('home1.php', array("blog"=>$blog, "slider"=>$slide));
		$this->load->view('footer.php');
	}
	
	function blog($id=null)
	{
		$blogku = $this->m_main->get_blogku($id);
		$recent = $this->m_main->get_recent();
		//var_dump($recent);
		$this->load->view('header.php');
		$this->load->view('blog.php', array("blog"=>$blogku, "rec"=>$recent));
		$this->load->view('footer.php');
	}
	
	function about_us()
	{
		$this->load->view('header.php');
		$this->load->view('about_us.php');
		$this->load->view('footer.php');
	}
}
