<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Agent extends MY_Controller {
	
	public function __construct()
	{
        // load library
		parent::__construct();
		$this->load->library('access');
		$this->load->library('form_validation');
		$this->load->model('m_admin');
    }
	
    public function index(){		
		$user = $this->access->get_user();
		$agent = $this->access->get_agent_id();
		$array = array();
		date_default_timezone_set("Asia/Makassar");
		$tm = date('Y');
		$i = 1;
		
		while($i < 13){
			$rata2 = $this->m_admin->ambil_penjualan_agent($i,$tm,$agent);
			$rata = round($rata2);
			array_push($array,$rata);
			$i++;
		}
		$this->load->view('admin_header2.php', array("user"=>$user));
		$this->load->view('v_agent.php', array("jual"=>$array,"tahun"=>$tm));
		$this->load->view('admin_footer.php');
		
    }

}
