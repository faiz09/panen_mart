<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller {
	
	public function __construct()
	   {
        // load library
		parent::__construct();
		$this->load->library('access');
		$this->load->library('form_validation');
		$this->load->model('m_product');
    }
	
    public function index() {		
		$user = $this->access->get_user();
		$produk = $this->m_product->get_product();
		$this->load->view('admin_header.php', array("user"=>$user));
		$this->load->view('v_product.php',array("produk"=>$produk));
		$this->load->view('admin_footer.php');
		
    }
	
	public function test() {		
		$category = $this->m_product->ambil_category();
		var_dump($category);
    }
	
	public function deactive(){
		$ids = $this->uri->segment(3);
		$this->m_product->deactive($ids);
		redirect('product');
    }
	
	public function tambah_product() {
		$user = $this->access->get_user();
		$categoryz = $this->m_product->ambil_category();
		
		$this->form_validation->set_rules('nama','Nama Produk','callback_cek_ada');
		//$this->form_validation->set_rules('harga','Harga Produk','callback_cek_ada');
		$this->form_validation->set_rules('satuan','Satuan','callback_cek_ada');
		$this->form_validation->set_rules('category','Category','callback_cek_ada');
		$this->form_validation->set_rules('status','Status','callback_cek_ada');
		
		if(empty($_FILES['file']['name']))
		{
			$this->form_validation->set_rules('file', 'File', 'callback_cek_ada');
		}
		$this->form_validation->set_rules('file_v','File_v','callback_cek_file');
		
		
		$this->load->view('admin_header.php', array("user"=>$user));
		if ($this->form_validation->run() == FALSE){
			 $this->load->view('v_product_tambah.php', array("category"=>$categoryz ));
		}else{
				
				$config['upload_path']          = dirname($_SERVER["SCRIPT_FILENAME"])."/component/upload/product";
				$config['allowed_types']        = 'jpeg|jpg|png';
				$config['max_size']             = 500;
				
				$this->load->library('upload', $config);
		 
				if (!$this->upload->do_upload('file')){
					
					$file = "";
				}else{
					$data_foto = $this->upload->data();
					$file = $data_foto['file_name'];
				}
				
				 $nama = htmlentities($this->input->post('nama'), ENT_QUOTES);
				 //$harga = htmlentities($this->input->post('harga'), ENT_QUOTES);
				 $id = acak_id("produk","id_produk");
				 $satuan = $this->input->post('satuan');
				 $category = $this->input->post('category');
				 $stat = $this->input->post('status');
				 date_default_timezone_set("Asia/Makassar");
				 $tgl = date("Y-m-d H:i:s");
				 //$url_title = url_title($judul);
				 $result=0;
				
					$result = $this->m_product->add_product(array(
					'id_produk' => $id['id'],
					'nama' => $nama,
					//'harga' => $harga,
					'satuan' => $satuan,
					'kategori' => $category,
					'gambar' => $file,
					'status' => $stat,
					'tanggal_iat' => $tgl));
					
				 if($result==1)
				 //echo json_encode(array("status"=>"sukses"));
				 redirect('product');
				 else
				 redirect('product/tambah_berita');
				//echo json_encode(array("status"=>"error"));
			 
		}
		
		$this->load->view('admin_footer.php');
		
    }
	
	public function cek_ada($str)
		{
			if($str == '' || $str == NULL)
			{
				$this->form_validation->set_message('cek_ada','<em>%s tidak boleh kosong</em>');
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
		
		public function cek_file($str)
		{
			if($str != '')
			{
				$this->form_validation->set_message('cek_file','<em>'.$str.'</em>');
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
	
	function delete($id=null){
	 $pic = $this->m_product->pic($id);
	 //var_dump($pic);
     $result=$this->m_product->delete($id);
	 $file = dirname($_SERVER["SCRIPT_FILENAME"])."/component/upload/product/".$pic->gambar;
	 //echo $file;
     if($result){
	 unlink($file);
	 echo json_encode(array("pesan"=>"Data berhasil dihapus!!",
                                        "status"=>"sukses"));
	}else{
     echo json_encode(array("pesan"=>"Data gagal dihapus!!",
                                        "status"=>"error"));
	}
   }
   
    function edit($id=null){
	 $user = $this->access->get_user();
     $dt = $this->m_product->edit_product($id);
	 $categoryz = $this->m_product->ambil_category();
	
	 $stat = $this->uri->segment(4);
	 if($stat){
			$stat1 = $stat;
		}else{
			$stat1 =0;
		}
	  $tgl_z = explode(" ",$dt->tanggal_iat);
	  $tgl_ku = date('Y/m/d', strtotime(str_replace('-','/',$tgl_z[0])));
	
		$this->form_validation->set_rules('nama','Nama Produk','callback_cek_ada');
		//$this->form_validation->set_rules('harga','Harga Produk','callback_cek_ada');
		$this->form_validation->set_rules('satuan','Satuan','callback_cek_ada');
		$this->form_validation->set_rules('category','Category','callback_cek_ada');
		$this->form_validation->set_rules('status','Status','callback_cek_ada');
		
	
	 $this->form_validation->set_rules('file_v','File_v','callback_cek_file');
	 
	 
        $this->load->view('admin_header', array("user"=>$user,"stat"=>$stat1));
		if ($this->form_validation->run() == FALSE){
		$this->load->view('v_product_edit.php', array("nama"=>$dt->nama,"tanggal"=>$tgl_ku,
													"pic"=>$dt->gambar,"stat"=>$dt->status,"ck"=>$stat1,
													"satuan"=>$dt->satuan,
													"category"=>$categoryz,"categoryq"=>$dt->kategori,
													"id"=>$id));
													
		}else{
				
				if(!empty($_FILES['file']['name']))
				{
					$config['upload_path']          = dirname($_SERVER["SCRIPT_FILENAME"])."/component/upload/product";
					$config['allowed_types']        = 'jpeg|jpg|png';
					$config['max_size']             = 100;
			 
					$this->load->library('upload', $config);
			 
					if (!$this->upload->do_upload('file')){
						$file = "";
					}else{
						$data_foto = $this->upload->data();
						$file = $data_foto['file_name'];
					}
				}else{
					$file = $dt->gambar;
				}
				 
				 $nama = htmlentities($this->input->post('nama'), ENT_QUOTES);
				 //$harga = htmlentities($this->input->post('harga'), ENT_QUOTES);
				 $satuan = $this->input->post('satuan');
				 $category = $this->input->post('category');
				 $stat = $this->input->post('status');
				 date_default_timezone_set("Asia/Makassar");
				 $tm = date('H:i:s');
				 $tgl =  $this->input->post('tanggal');
				 $tgl_f = date('Y-m-d H:i:s', strtotime(str_replace('-','/',$tgl." ".$tm)));
			
				 $result=0;
				
					$data1 = array(
					'nama' => $nama,
					'tanggal_iat' => $tgl_f,
					'gambar' => $file,
					'status' => $stat,
					//'harga' => $harga,
					'satuan' => $satuan,
					'kategori' => $category);
					
					$result = $this->m_product->update_product($data1, $id);
					
				 if($result==1)
				 //echo json_encode(array("status"=>"sukses"));
				 redirect('product');
				 else
				 redirect('product/edit/$id/3');
				//echo json_encode(array("status"=>"error"));
			 
		}
		
		$this->load->view('admin_footer.php');
   }
}
