<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends MY_Controller {
	
	public function __construct()
	   {
        // load library
		parent::__construct();
		$this->load->library('access');
		$this->load->library('form_validation');
		$this->load->model('m_member');
    }
	
    public function index(){		
		$user = $this->access->get_user();
		$member = $this->m_member->get_member();
		$this->load->view('admin_header.php', array("user"=>$user));
		$this->load->view('v_member.php',array("member"=>$member));
		$this->load->view('admin_footer.php');
		
    }
	
	public function deactive(){
		$ids = $this->uri->segment(3);
		//$this->m_member->deactive($ids);
		redirect('member');
    }
	
	public function cek_ada($str)
		{
			if($str == '' || $str == '0' || $str == NULL)
			{
				$this->form_validation->set_message('cek_ada','<em>%s tidak boleh kosong</em>');
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
		
		public function cek_file($str)
		{
			if($str != '')
			{
				$this->form_validation->set_message('cek_file','<em>'.$str.'</em>');
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
	
	function delete($id=null){
	 $pic = $this->m_member->pic($id);
	 //var_dump($pic);
     $result=$this->m_member->delete($id);
	 $file = dirname($_SERVER["SCRIPT_FILENAME"])."/component/upload/user/".$pic->foto;
	 //echo $file;
     if($result){
	 unlink($file);
	 echo json_encode(array("pesan"=>"Data berhasil dihapus!!",
                                        "status"=>"sukses"));
	}else{
     echo json_encode(array("pesan"=>"Data gagal dihapus!!",
                                        "status"=>"error"));
	}
   }
   
    function edit($id=null){
	 $user = $this->access->get_user();
     $dt = $this->m_member->edit_member($id);
	
	 $stat = $this->uri->segment(4);
	 if($stat){
			$stat1 = $stat;
		}else{
			$stat1 =0;
		}
	  $tgl_z = explode(" ",$dt->tanggal_iat);
	  $tgl_ku = date('Y/m/d', strtotime(str_replace('-','/',$tgl_z[0])));
	
		$this->form_validation->set_rules('nama','Nama Member','callback_cek_ada');
		$this->form_validation->set_rules('telepon','Nomor Telepon','callback_cek_ada');
		$this->form_validation->set_rules('kelamin','Jenis Kelamin','callback_cek_ada');
		$this->form_validation->set_rules('email','Email','callback_cek_ada');
		$this->form_validation->set_rules('alamat','Alamat','callback_cek_ada');
		$this->form_validation->set_rules('lat','Latitude','callback_cek_ada');
		$this->form_validation->set_rules('lon','Longitude','callback_cek_ada');
		$this->form_validation->set_rules('status','Status','callback_cek_ada');
		$this->form_validation->set_rules('tanggal','Tanggal','callback_cek_ada');
		
	
		$this->form_validation->set_rules('file_v','File_v','callback_cek_file');
	 
	 
        $this->load->view('admin_header', array("user"=>$user,"stat"=>$stat1));
		if ($this->form_validation->run() == FALSE){
		$this->load->view('v_member_edit.php', array("nama"=>$dt->nama,"tanggal"=>$tgl_ku,
													"pic"=>$dt->foto,"stat"=>$dt->flag,"ck"=>$stat1,
													"telepon"=>$dt->telepon,"kelamin"=>$dt->kelamin,
													"email"=>$dt->email,"lat"=>$dt->lat,
													"lon"=>$dt->lon,"level"=>$dt->level,"alamat"=>$dt->alamat,
													"id"=>$id));
													
		}else{
				
				if(!empty($_FILES['file']['name']))
				{
					$config['upload_path']          = dirname($_SERVER["SCRIPT_FILENAME"])."/component/upload/user";
					$config['allowed_types']        = 'jpeg|jpg|png';
					$config['max_size']             = 500;
			 
					$this->load->library('upload', $config);
			 
					if (!$this->upload->do_upload('file')){
						$file = "";
					}else{
						$data_foto = $this->upload->data();
						$file = $data_foto['file_name'];
						$file_delete = dirname($_SERVER["SCRIPT_FILENAME"])."/component/upload/user/".$dt->foto;
						unlink($file_delete);
					}
				}else{
					$file = $dt->gambar;
				}
				 
				 $nama = htmlentities($this->input->post('nama'), ENT_QUOTES);
				 $telepon = htmlentities($this->input->post('telepon'), ENT_QUOTES);
				 $kelamin = $this->input->post('kelamin');
				 $email = $this->input->post('email');
				 $alamat = $this->input->post('alamat');
				 $lat = $this->input->post('lat');
				 $lon = $this->input->post('lon');
				 $stat = $this->input->post('status');
				 date_default_timezone_set("Asia/Makassar");
				 $tm = date('H:i:s');
				 $tgl =  $this->input->post('tanggal');
				 $tgl_f = date('Y-m-d H:i:s', strtotime(str_replace('-','/',$tgl." ".$tm)));
			
				 $result=0;
				
					$data1 = array(
					'nama' => $nama,
					'tanggal_iat' => $tgl_f,
					'foto' => $file,
					'flag' => $stat,
					'telepon' => $telepon,
					'kelamin' => $kelamin,
					'alamat' => $alamat,
					'lat' => $lat,
					'lon' => $lon,
					'email' => $email);
					
					$result = $this->m_member->update_member($data1, $id);
					
				 if($result==1)
				 //echo json_encode(array("status"=>"sukses"));
				 redirect('member');
				 else
				 redirect('member/edit/$id/3');
				//echo json_encode(array("status"=>"error"));
			 
		}
		
		$this->load->view('admin_footer.php');
   }
   /*
   public function tambah_member() {
		$user = $this->access->get_user();
		
		$this->form_validation->set_rules('nama','Nama Produk','callback_cek_ada');
		$this->form_validation->set_rules('harga','Harga Produk','callback_cek_ada');
		$this->form_validation->set_rules('satuan','Satuan','callback_cek_ada');
		$this->form_validation->set_rules('category','Category','callback_cek_ada');
		$this->form_validation->set_rules('status','Status','callback_cek_ada');
		
		if(empty($_FILES['file']['name']))
		{
			$this->form_validation->set_rules('file', 'File', 'callback_cek_ada');
		}
		$this->form_validation->set_rules('file_v','File_v','callback_cek_file');
		
		
		$this->load->view('admin_header.php', array("user"=>$user));
		if ($this->form_validation->run() == FALSE){
			 $this->load->view('v_product_tambah.php', array("category"=>$categoryz ));
		}else{
				
				$config['upload_path']          = dirname($_SERVER["SCRIPT_FILENAME"])."/component/upload/product";
				$config['allowed_types']        = 'jpeg|jpg|png';
				$config['max_size']             = 500;
				
				$this->load->library('upload', $config);
		 
				if (!$this->upload->do_upload('file')){
					
					$file = "";
				}else{
					$data_foto = $this->upload->data();
					$file = $data_foto['file_name'];
				}
				
				 $nama = htmlentities($this->input->post('nama'), ENT_QUOTES);
				 $harga = htmlentities($this->input->post('harga'), ENT_QUOTES);
				 $id = acak_id("produk","id_produk");
				 $satuan = $this->input->post('satuan');
				 $category = $this->input->post('category');
				 $stat = $this->input->post('status');
				 date_default_timezone_set("Asia/Makassar");
				 $tgl = date("Y-m-d H:i:s");
				 //$url_title = url_title($judul);
				 $result=0;
				
					$result = $this->m_product->add_product(array(
					'id_produk' => $id['id'],
					'nama' => $nama,
					'harga' => $harga,
					'satuan' => $satuan,
					'kategori' => $category,
					'gambar' => $file,
					'status' => $stat,
					'tanggal_iat' => $tgl));
					
				 if($result==1)
				 //echo json_encode(array("status"=>"sukses"));
				 redirect('product');
				 else
				 redirect('product/tambah_berita');
				//echo json_encode(array("status"=>"error"));
			 
		}
		
		$this->load->view('admin_footer.php');
		
    }
   */
}
