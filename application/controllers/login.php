<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	
	public function __construct()
	{
        // load library
		parent::__construct();
		$this->load->library('access');
		$this->load->library('form_validation');
		$this->load->library('session');
		//$this->load->library('myencryption');
    }
	
    public function index(){
	 /*$username=$this->input->post('signin-user');
     $password=$this->input->post('signin-password');
     if($username){
       $login = $this->access->login($username, $password);
       if($login==true)redirect('admin');
     }*/
			$this->form_validation->set_rules('signin-user','Username Kosong','callback_cek_user');
			
			$stat= $this->input->post('stat');
			//echo "</br></br>";
			//var_dump($agent);
			//$this->form_validation->set_rules('signin-password','Password Kosong','callback_cek_pass');
			
			if ($this->form_validation->run() == FALSE) {
				 $this->load->view('login.php');
			}else{
				 if($stat == 'on'){
					redirect('admin');
				 }else{
					redirect('agent');
				 }
			}
    }
	
	/*function cekuser(){
		$cek = $this->input->post('value');
		//$cek = "aa";
		$this->load->model('mlogin');
		//$dataz = $this->model_barang->list_barangz($cek)->result();
		echo json_encode(array('comz'=>$this->mlogin->cek_user($cek)));
	}*/
	
	function cekuser($usr){
		$this->load->model('mlogin');
		return $this->mlogin->cek_user($usr);
	}
	
	function cekuser_agent($usr){
		$this->load->model('mlogin');
		return $this->mlogin->cek_user_agent($usr);
	}
	
	public function cek_user($user)
	{
		$pass=$this->input->post('signin-password');
		$adm= $this->input->post('stat');
		if ($user == '' && $pass == '')
		{
			$this->form_validation->set_message('cek_user','<span class="element-right label label-warning"><i class="fa fa-times-circle-o"></i>&nbsp;&nbsp;Username & Password Tidak Boleh Kosong </span>');
			return FALSE;
			
		}else if($user == '' && $pass != ''){
		
			$this->form_validation->set_message('cek_user','<span class="element-right label label-warning"><i class="fa fa-times-circle-o"></i>&nbsp;&nbsp;Username Tidak Boleh Kosong </span>');
			return FALSE;
			
		}else if($user != '' && $pass == ''){
		
			$this->form_validation->set_message('cek_user','<span class="element-right label label-warning"><i class="fa fa-times-circle-o"></i>&nbsp;&nbsp;Password Tidak Boleh Kosong </span>');
			return FALSE;
		
		}else if($user != '' && $pass != ''){
			
			if($adm == 'on'){
				$ck = $this->cekuser($user);
				if($ck == True){
					 $login = $this->access->login($user, $pass);
					 if($login==true){
							return TRUE;
						}else{
							$this->form_validation->set_message('cek_user','<span class="element-right label label-warning"><i class="fa fa-times-circle-o"></i>&nbsp;&nbsp;Password Admin Tidak Valid </span>');
							return FALSE;
							//redirect('Login');
						}
				}else{
					$this->form_validation->set_message('cek_user','<span class="element-right label label-warning"><i class="fa fa-times-circle-o"></i>&nbsp;&nbsp;Username & Password Admin Tidak Valid </span>');
					return FALSE;
				}
			}else if($adm == ''){
				$ck = $this->cekuser_agent($user);
				if($ck == True){
					 $login = $this->access->login_agent($user, $pass);
					 if($login==true){
							return TRUE;
						}else{
							$this->form_validation->set_message('cek_user','<span class="element-right label label-warning"><i class="fa fa-times-circle-o"></i>&nbsp;&nbsp;Password Agent Tidak Valid </span>');
							return FALSE;
							//redirect('Login');
						}
				}else{
					$this->form_validation->set_message('cek_user','<span class="element-right label label-warning"><i class="fa fa-times-circle-o"></i>&nbsp;&nbsp;Username & Password Agent Tidak Valid </span>');
					return FALSE;
				}
			}
		}
	}
	
	function coba(){
		$res = $this->access->is_login();
		$res2 = $session = $this->session->userdata('kode');
		$extr = $this->myencryption->decode($res2);
		$dt = explode('/',$extr);
		$pass = md5('hendra');
		$sap = count($dt);
		var_dump($res);
		echo "</br>";
		var_dump($res2);
		echo "</br>";
		var_dump($extr);
		echo "</br>";
		var_dump($sap);
		echo "</br> passnya adalah :";
		var_dump($pass);
	}
	
	function logout()
    {
      $this->access->logout();
      $this->index();
    }

}
