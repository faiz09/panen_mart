<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MY_Controller {
	
	public function __construct()
	   {
        // load library
		parent::__construct();
		$this->load->library('access');
		//$this->load->library('form_validation');
		$this->load->model('m_order');
    }
	
    public function index() {		
		$order = $this->m_order->get_order();
		foreach($order as $ord){
			$cr =  explode(',',$ord->cart);
			$ag =  explode('/',$cr[0]);
			$agent = $this->m_order->get_agentq($ag[0]);
			$ord->agent = $agent;
			//var_dump($agent);
		}
		$user = $this->access->get_user();
		$this->load->view('admin_header.php', array("user"=>$user));
		$this->load->view('v_order.php', array("order"=>$order));
		$this->load->view('admin_footer.php');
		
    }
	
	public function detail($id=null){		
		$detail = $this->m_order->get_order_detail($id);
		//$barangx = "23916704/2,79253817/4";
		$cart = explode(',',$detail[0]->cart);
		$ag =  explode('/',$cart[0]);
		$agent = $this->m_order->get_agentq($ag[0]);
		$detail[0]->agent = $agent;
		//$cart = explode(',',$barangx);
		$all = array();
		foreach($cart as $barang){
			$br = explode('/',$barang);
			$barangku = $this->m_order->get_nama_barang($br[0]);
			$jumlah = $br[1];
			$barangku['jumlah'] = $jumlah;
			$all[] = $barangku;
		}
		//var_dump($agent);
		$user = $this->access->get_user();
		$this->load->view('admin_header.php', array("user"=>$user));
		$this->load->view('v_order_detail.php', array("detail"=>$detail,"cartq"=>$all));
		$this->load->view('admin_footer.php');
    }
	
	 public function test_id() {		
		//$id = acak_id("berita","id_berita");
		//var_dump($id);
		$id = $this->uri->segment(3);
		$k = $this->m_order->cek_status_metode($id);
		var_dump($k);
		echo '<br>';
		$g = $this->m_order->cek_status_struk($id);
		var_dump($g);
    }
	
	public function change_to(){
		$id = $this->uri->segment(3);
		$val = $this->uri->segment(4);
		//$urutan1 = $this->m_category->ambil_urutan();
		
		if($val == "completed"){
			$valku = '2';
			$this->m_order->update_status($id,$valku);
		}else if($val == "processing"){
			$cek1 = $this->m_order->cek_status_metode($id);
			if($cek1 == '2'){
				$valku = '0';
				$this->m_order->update_status($id,$valku);
			}else if($cek1 == '1'){
				$cek2 = $this->m_order->cek_status_struk($id);
				if($cek2 != ''){
					$valku = '1';
					$this->m_order->update_status($id,$valku);
				}else{
					$valku = '0';
					$this->m_order->update_status($id,$valku);
				}
			}
		}
		//echo $urutan1[$c_with];
		//$this->m_category->swap_all($urutan1[$stat], $urutan1[$c_with]);
		
		redirect('order');
    }
}
