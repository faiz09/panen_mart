<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Access
{
	public $user;

	function __construct()
	{
		$this->CI =& get_instance();
		$auth = $this->CI->config->item('auth');
		//$this->CI->load->helper('my_libs');
		$this->CI->load->library('session');
		$this->CI->load->model('mlogin');
		$this->mlogin =& $this->CI->mlogin;
		$this->CI->load->library('myencryption');
	}
	
	function login($username, $password)
	{
		$result = $this->mlogin->get_user($username);
		$sq = compare_hash($password, $result->userPassword, false);
		if($sq == true){
			if(!empty($result))
			{
				//$this->CI->session->set_userdata('kode',$this->CI->myencryption->encode($result->userAccount));
				//return TRUE;
				date_default_timezone_set("Asia/Makassar");
				$session = md5(date('Y/m/d his').rand(0,999999999));
				$res = $this->mlogin->save_session($session,$result->userAccount,'admin');
				if ($res == TRUE)
				{
					$this->CI->session->set_userdata('session',$session);
					$this->CI->session->set_userdata('kode',$this->CI->myencryption->encode($result->userAccount.'/admin'));
					return TRUE;
				} else
				{
				return FALSE;
				}
			}else{
			return FALSE;
			}
		}else{
			return FALSE;
		}
	}
	
	function login_agent($username, $password)
	{
		
		$result = $this->mlogin->get_user_agent($username);
		$sq = compare_hash($password, $result->member_pass, false);
		if($sq == true){
			if(!empty($result))
			{
				//$this->CI->session->set_userdata('kode',$this->CI->myencryption->encode($result->userAccount));
				//return TRUE;
				date_default_timezone_set("Asia/Makassar");
				$session = md5(date('Y/m/d his').rand(0,999999999));
				$res = $this->mlogin->save_session($session,$result->member_acount,'agent');
				if ($res == TRUE)
				{
					$this->CI->session->set_userdata('session',$session);
					$this->CI->session->set_userdata('kode',$this->CI->myencryption->encode($result->member_acount.'/agent'));
					return TRUE;
				} else
				{
				return FALSE;
				}
			}else{
			return FALSE;
			}
		}else{
			return FALSE;
		}
	}

	function is_login()
	{
		$session = $this->CI->session->userdata('session');

		$kode = $this->CI->session->userdata('kode');
		$extr = $this->CI->myencryption->decode($kode);
		$dt = explode('/',$extr);
		if (count($dt) == 2)
		{
			$user = $dt[0];
			$group = $dt[1];
			return $this->mlogin->is_login($session,$user,$group);
		} else
		{
			return FALSE;
		}
	}

	function get_user()
	{
		$kode = $this->CI->session->userdata('kode');
		$extr = $this->CI->myencryption->decode($kode);
		
		//return $this->mlogin->get_user_nama($extr);
		$dt = explode('/',$extr);
		if (count($dt) == 2)
		{
			$user = $dt[0];
			$group = $dt[1];
			if($group == 'admin'){
				return $this->mlogin->get_user_nama($user);
			}else if($group == 'agent'){
				return $this->mlogin->get_user_nama_agent($user);
			}
			//return $user;
		} else
		{
			return '';
		}
	}
	
	function get_agent_id()
	{
		$kode = $this->CI->session->userdata('kode');
		$extr = $this->CI->myencryption->decode($kode);
		
		$dt = explode('/',$extr);
		if (count($dt) == 2)
		{
			$user = $dt[0];
			$group = $dt[1];
			if($group == 'admin'){
				return '';
			}else if($group == 'agent'){
				return $this->mlogin->get_id_agent($user);
			}
		} else
		{
			return '';
		}
	}
	
	function get_admin_id()
	{
		$kode = $this->CI->session->userdata('kode');
		$extr = $this->CI->myencryption->decode($kode);
		
		$dt = explode('/',$extr);
		if (count($dt) == 2)
		{
			$user = $dt[0];
			$group = $dt[1];
			if($group == 'admin'){
				return $this->mlogin->get_id_admin($user);
			}else if($group == 'agent'){
				return '';
			}
		} else
		{
			return '';
		}
	}

	function logout()
	{
		$this->CI->session->unset_userdata('session');
		$this->CI->session->unset_userdata('kode');
	}
	
	function is_who($modul)
	{
		$kode = $this->CI->session->userdata('kode');
		$extr = $this->CI->myencryption->decode($kode);
		
		//return $this->mlogin->get_user_nama($extr);
		$dt = explode('/',$extr);
		if(count($dt) == 2)
		{
			$group = $dt[1];
			return $group;
			//return $user;
		} else
		{
			return '';
		}
	}
	
	function testq($password)
	{
		$pass = do_hash($password,'bcrypt');
		return $pass;
	}

}
